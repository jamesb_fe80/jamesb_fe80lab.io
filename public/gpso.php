<?php
$digitclip=3;
header('Content-Type: text/event-stream');
header('Access-Control-Allow-Origin: *');
header('Cache-Control: no-cache');

function SSEsend($in){
	printf("data: %s\n\n", json_encode($in));
	// ob_flush();
	flush();
}

$toclip=['magtrack', 'track','lat','lon','alt','epx','epy','epv','eps','epd','epc','ept','speed','climb'];
error_reporting(E_ALL);
$tpvs=['time', 'mode', 'status'];

$tag = "/";
if(isset($_SERVER)&&isset($_SERVER['REQUEST_METHOD'])&&($_SERVER['REQUEST_METHOD']=="GET")&&(isset($_SERVER['PATH_INFO']))){
        $tag=preg_replace(["/\n\n*/"], ["/"], $_SERVER['PATH_INFO']);
 }

if (($socket=socket_create(AF_INET, SOCK_STREAM, SOL_TCP)) and (socket_connect($socket, "127.0.0.1", 2947))) {
    $fluff=socket_read($socket,2048,PHP_NORMAL_READ);

    $file = file_get_contents('/tmp/gpsd-c.json');
    $points = json_decode($file);
    $bleh = ["class" => "inital_tracking", "points" => $points];
    SSEsend($bleh);

    socket_write($socket,"?VERSION;\n");
    $bleh = ["enable" => true, "json" => true];
    if(($tag != "/")&&($tag != "")) {
       $bleh["device"] = $tag;
    }
    socket_write($socket,"?WATCH=".json_encode($bleh)."\n");
    while(1) {
	$escape=3;
	while ($escape>0) {
	    $fluff=socket_read($socket,2048,PHP_NORMAL_READ);
	    if (strlen($fluff) < 9) {
		continue;
	    }
	    $bleh=json_decode($fluff,true);
	    switch($bleh['class']) {
	    	case "SKY":
		    // DOPs x y v t h g p
                    $dop = $bleh;
		    $dop['class'] = 'sky';
		    foreach(["x", "y", "v", "t", "h", "g", "p"] as $i) {
			$key = $i."dop";
			$dop[$key] = round($bleh[$key], $digitclip);
		      }
		    SSEsend($dop);
		    break;
		case "TPV":
                    $outy = $bleh;
		    $outy['class'] = 'tpv';
		    foreach($toclip as $fog){
			if(isset($bleh[$fog])) {
			    $outy[$fog]=round($bleh[$fog], $digitclip);
			  }
		      }
		    SSEsend($outy);
		    break;
		default:
		    if(isset($bleh['class'])) {
		        SSEsend($bleh);
		      }
		    break;
	      }
	  }
      }
    socket_close($socket);
  }

?>
