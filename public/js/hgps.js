"use strict";
var gap = 20,
	url = "https://dell-2018.jamesb192.com/cgi-bin/gpso-3.py",
	url_pulling = false,
	url_source,
	sort_order=["svid", "gnssid", "-used"],
	back_store = {"ring":[]},
	ring_update = true,
	ring_entries = 3,
	horizon = -10,
	scalar = 230,
	sky_view=[[],[],[]],
	shim_len = 1,
	shim_move = 1,
	latlon_format = 1,
	from, spots, jsob, back, bod, prn, arrow;
var scopebands = 3,
	dx = 0,
	dy = 0;
var tracks = {},
	tracks_old = {},
	sats = [],
	sats_old = [],
	staled = {},
	debug = {};
// 45 37 32 20 5 // don't you love it when you forget what your notes are for?

function radians_to_degrees(radians) {
	return radians * 180 / Math.PI;
}

function degrees_to_radians(degrees) {
	return degrees / 180 * Math.PI;
}

function empty(grandparent) {
	for (var count=grandparent.childElementCount-1;count >= 0;count --) {
		grandparent.removeChild(grandparent.childNodes[count]);
	}
}
function remove(child) {
	child.parentElement.removeChild(child);
}

function paint_scope() {
	var d = "", i, k, step, t, bb, xy, rm = document.querySelector("#ringmark");
	empty(rm);
	if ((scopebands <= 6)&&(scopebands >= 1)) {
		for(step=0;step<=90-horizon;step+=90/scopebands) {
			i = polar_to_cart(180,step,scalar).y;
			k = `A${i} ${i} 0 0 0 0,`;
			d += `M0,${i}${k}${-i}${k}${i}Z`;
			if (step < 95) {
				xy = polar_to_cart(45, step, scalar);
				t = jqmlsvg([rm, ["text", {"x": xy.x, "y": xy.y}, `${digits(step,1)}°`]]);
			}
		}
	}
	i=gap+scalar;
	k = `A${i} ${i} 0 0 0 0,`;
	d += `M0,${i}${k}${-i}${k}${i}Z`;
	d+=`M${-i},0H${i}ZM0,${-i}V${i}Z`;

	jqmlsvg([document.querySelector("#scope"), {"d": d}]);
	xy = polar_to_cart(0, 0, scalar);
	jqmlsvg([rm, ["text", {"x": xy.x, "y": xy.y}, "N"]]);
	xy = polar_to_cart(270, 0, scalar);
	jqmlsvg([rm, ["text", {"x": xy.x, "y": xy.y}, "E"]]);
	xy = polar_to_cart(90, 0, scalar);
	jqmlsvg([rm, ["text", {"x": xy.x, "y": xy.y}, "W"]]);
	xy = polar_to_cart(180, 0, scalar);
	jqmlsvg([rm, ["text", {"x": xy.x, "y": xy.y}, "S"]]);
}

function init() {
	var d, e, i, j, k;
	bod = document.querySelector("body");
	local_fetch();
	document.querySelector("[name=url]").value = url;

	if(!url_pulling) {
		events();
	}

	from = document.querySelector("form");
	arrow = document.querySelector("#arrow");
	prn = document.querySelector("#prn");
	spots = document.querySelector("#spots");

	document.querySelector("#tabsky thead").addEventListener("click", function (e) {
		resort(e)
	});
	document.querySelector("#navbar").addEventListener("input", function (e) {
		stylize2(e)
	});
	document.querySelector("#navbar").addEventListener("click", function (e) {
		stylize(e)
	});
	spots.addEventListener("mouseout", function (e) {
		document.querySelector("#tabtip").setAttribute("style", " ");
	});
	spots.addEventListener("mouseover",  function (e) {
		var id = e.target.textContent;
		if (id != "") {
			var table = prn;
			for (var nrow = table.rows.length - 1; nrow >= 0; nrow--) {
				var row = table.rows[nrow];
				if ((row.cells[1].innerHTML.toString() == id)&&(e.target.parentNode.classList.value.includes(row.cells[0].innerHTML.toLowerCase()))) {
					paint_tip(e, row);
					return;
				}
			}
		}
	});
}

function paint_tip(e, row) {
	var tmp, HTML = e.target.innerHTML;
	var celestial = {"M": "The Moon", "S": "The Sun"};
	if (is_undefined(celestial[HTML])) {
		tmp = row.cells[0].innerHTML;
		tmp += ": " + row.cells[1].innerHTML;
		tmp += "\nSNR: " + row.cells[2].innerHTML;
		if (row.cells[3].innerHTML != "") {
			tmp += "\nUsed: Yes!";
		} else {
			tmp += "\nUsed: No.";
		}
	} else {
		tmp = celestial[HTML];
	}
	document.querySelector("#tip0").innerHTML = tmp;
	document.querySelector("#tip4").innerHTML = row.cells[4].innerHTML;
	document.querySelector("#tip5").innerHTML = row.cells[5].innerHTML;
	var lefty = e.pageX - document.querySelector("#tabtip").clientWidth - 45;
	var toppy = e.pageY - document.querySelector("#tabtip").clientHeight / 2 - 9;
	var styled = "visibility:visible;top:" + toppy + "px;left:" + lefty + "px;";
	document.querySelector("#tabtip").setAttribute("style", styled);
	return;
}

function repaint(myclass) {
	if (is_defined(myclass)) {
		switch (myclass) {
			case "tpv":
				paint_tpv();
				doEP();
				paint_celes();
				break;
			case "sky":
				doDOP();
				if (!('satellites' in jsob)) {
					break;
				}
				tracks_old = tracks;
				jsob.satellites.forEach(function(sat){
					var id = sat.PRN;
					if (is_defined(staled[id])) {
						staled[id] -= 1;
					} else {
						staled[id] = 0;
					}
				});
				Object.keys(tracks_old).forEach(function(e){
					if (is_undefined(staled[e])) {
						staled[e] = 1;
					} else {
						staled[e] += 1;
					}
					if (staled[e] > 10) {
						delete staled[e];
						delete tracks[e];
					}
				});
				paint_sky1();
				paint_sky2();
				center_text_spot_resize();
				break;
			case "TOFF":
			case "PPS":
				var diff = back_store[myclass].real_sec - back_store[myclass].clock_sec;
				if(is_undefined(back_store[myclass].clock_nsec)) {
					diff += (back_store[myclass].real_musec - back_store[myclass].clock_musec)/1e6;
					diff  = diff.toFixed(6)
				} else {
					diff += (back_store[myclass].real_nsec - back_store[myclass].clock_nsec)/1e9;
					diff  = diff.toFixed(9)
				}
				from[myclass].value = diff;
				break;
			case "VERSION":
				var fields = ["proto_major", "proto_minor", "release", "rev"];
				fields.forEach(function(field){
					if(is_defined(back_store.VERSION[field])) {
						from[`version_${field}`].value = back_store.VERSION[field];
					} else {
						from[`version_${field}`].value = "";
					}
				});
				break;
			case "DEVICES":
				var fields = ["path", "driver", "subtype"];
				fields.forEach(function(field){
					if(is_defined(back_store.DEVICES.devices[0][field])) {
						from[`device_${field}`].value = back_store.DEVICES.devices[0][field];
					} else {
						from[`device_${field}`].value = "";
					}
				});
				break;
			case "DEVICE":
				var fields = ["driver", "subtype"];
				fields.forEach(function(field){
					if(is_defined(back_store.DEVICE[field])) {
						from[`device_${field}`].value = back_store.DEVICE[field];
					} else {
						from[`device_${field}`].value = "";
					}
				});
				break;
			case "inital_tracking":
				tracks = back_store.inital_tracking.points;
				Object.keys(tracks).forEach(function(e){ // Filter out silly data points
					tracks[e] = tracks[e].filter(function(f){
						return ((f[0]>=-20)&&(f[0]<=90)&&(f[1]>=0)&&(f[1]<360));
					});
				});
				back_store.tracks = tracks;
				break;
		}
	}
}

function paint_tpv() {
	var d, e, m, s, z, ref, list = ["lat", "lon", "track", "epd"], dowp=["Sun","Mon","Tues","Wednes","Thurs","Fri","Satur"];

	d = new Date(back_store.tpv.time);
	e = d.toString().split(" ");
//	f = Intl.DateTimeFormat.prototype.formatToParts(d)

	from["time"].value = e[4] + (d.getHours() < 12 ? " AM" : " PM");
	from["date"].value = `${dowp[d.getDay()]}day ${e[1]} ${e[2]} ${e[3]}`;

	if(("number" == typeof(back_store.tpv.track)) && ("number" == typeof(back_store.tpv.magtrack))) {
                d = 0;
                // d = back_store.tpv.track - back_store.tpv.magtrack;
                // d = back_store.tpv.magtrack;
	} else { d = 0; }
	s = document.querySelector("#css").sheet.cssRules;
	s[0].style.transform = `rotate(${-d}deg)`;
	s[1].style.transform = `rotate(${d}deg)`;
	for (ref in list) {
		if (is_defined(back_store.tpv[list[ref]])) {
			d = back_store.tpv[list[ref]];
			switch(latlon_format) {
				case 2:
					m = (d%1) * 60;
					d = Math.floor(d);
					if (d < 0) { d += 1; }
					if (m < 0) { m = -m; }
					from[list[ref]].value = `${d}°${m.toFixed(2)}\'`;
					break;
				case 3:
					m = (d%1) * 60;
					d = Math.floor(d);
					s = (m%1) * 60;
					m = Math.floor(m);

					if (d < 0) { d += 1; }
					if (m < 0) { m = -1-m; }
					if (s < 0) { s = -s; }
					from[list[ref]].value = `${d}°${m}\'${s.toFixed(2)}\"`;
					break;
				default:
					from[list[ref]].value = `${d}°`;
			}
		}
	}
	var ref, list = ["altHAE", "altMSL", "epx", "epy", "epv", "ecefx", "ecefy", "ecefz", "ecefpAcc"]
	for (ref in list) {
		if (is_defined(back_store.tpv[list[ref]])) {
			from[list[ref]].value = (back_store.tpv[list[ref]] * shim_len).toFixed(2);
		}
	}
	var ref, list = ["speed", "climb", "eps", "epc", "ecefvx", "ecefvy", "ecefvz", "ecefvAcc"]
	for (ref in list) {
		if (is_defined(back_store.tpv[list[ref]])) {
			from[list[ref]].value = (back_store.tpv[list[ref]] * shim_move).toFixed(2);
		}
	}
	from.tzone.value = Intl.DateTimeFormat().resolvedOptions().timeZone;
	if (is_defined(back_store.tpv.track)) {
		if (is_defined(back_store.tpv.epd)) {
			ref = back_store.tpv.epd;
		} else {
			ref = 0;
		}
		ref = polar_to_cart(-ref, (horizon > 0 ? horizon : 0), 20);
		ref = `M0,0L${-ref.x},${ref.y}A20,20 0,0,0 ${ref.x},${ref.y}L0,0V-20Z`
		jqml([arrow, {"transform": `rotate(${back_store.tpv.track})`, "d": ref}]);
	}
	if ((back_store.tpv.mode >= 0) && (back_store.tpv.mode <4 )) {
		from['tpv_mode'].value = ["not yet fixed", "no fix", "2d fix", "3d fix"][back_store.tpv.mode];
	} else {
		from['tpv_mode'].value = ""
	}
	if(is_defined(back_store.tpv.status)) {
		from['tpv_status'].value = back_store.tpv.status;
	} else {
		from['tpv_status'].value = "";
	}
}

function paint_sky2() {
	sky_view = [[],[],sky_view[2]]
	var dump = common_a_b(sats, sats_old);
	dump[2].forEach(function (e) {
		// Mark expiring entries for removal
		document.querySelector("#t" + e).setAttribute("class", "d");
	});
	var my_dict = tr_struct_from_sat()
	dump[1].forEach(function (e) {
		// Add the filled table row
		var tr, my_arr = my_dict[e];
		tr = jqml(["tr", {"class":"a","id":`t${e}`},["td"],["td"],["td"],["td"],["td"],["td"]]);
		for (var count = 5; count >= 0; count --) {
			tr.cells[count].innerText = my_dict[e][count];
		}
		jqml([prn, [tr]]);
	});
	dump[0].forEach(function (e) {
		// Update the filled table row
		var tr, my_arr = my_dict[e];
		tr = document.querySelector(`#t${e}`);
		for (var count = 5; count >= 0; count --) {
			if(tr.cells[count].innerText != my_dict[e][count]) {
				tr.cells[count].innerText = my_dict[e][count];
				tr.cells[count].setAttribute("class", "u");
			}
		}
	});

	sort();

	setTimeout(function(){
		dump[2].forEach(function (e) {
			remove(document.querySelector(`#t${e}`));
		});
		document.querySelectorAll("tr.a").forEach(function(node){
			node.classList.remove("a");
		});
		document.querySelectorAll("td.u").forEach(function(node){
			node.classList.remove("u");
		});
	}, 701);
}

function tr_struct_from_sat() {
	var inner, key, ta = {}, tr;
	back_store.sky.satellites.forEach(function(sat){

		key = `t${new_id_from_sat(sat)}`;
		tr = [];
		["gnssid", "svid", "ss", "used", "el", "az"].forEach(function(iter){
			key = new_id_from_sat(sat)
			switch(iter) {
				case "gnssid":
					inner = "UN";
					if (is_undefined(sat.gnssid)) {
						inner = "OB";
					} else if ((sat.gnssid < 7) && (sat.gnssid >= 0)) {
						inner = ["GP", "SB", "GA", "BD", "IM", "QZ", "GL"][sat.gnssid];
					}
					break;
				case "used":
					inner = sat[iter] ? "\u221a" : "";
					break;
				case "svid":
					inner = is_defined(sat.PRN) ? sat.PRN : "-1";
					inner = is_defined(sat[iter]) ? sat.svid : inner;
					break;
				default:
					inner = is_defined(sat[iter]) ? sat[iter] : "";
			}
			tr.push(inner);
		});
		ta[key]= tr;
	});
	return ta;
}

function doDOP() {
	//  DOPs x y v t h g p
	var list = ["xdop", "ydop", "vdop", "tdop", "hdop", "gdop", "pdop"];
	for (var ref in list) {
		if (is_defined(back_store.sky[list[ref]])) {
			from[list[ref]].value = back_store.sky[list[ref]].toFixed(2);
		} else {
			from[list[ref]].value = "";
		}
	}
}

function doEP() {
	var list = ["epx", "epy", "epe", "epv", "eps", "epc", "ept"];
	for (var ref in list) {
		name = "t_"+list[ref];
		if(is_defined(back_store.tpv[list[ref]])){
			from[name].value = back_store.tpv[list[ref]].toFixed(2);
		} else {
			from[name].value = " ";
		}
	}
}

function paint_sky1() {
	var stile, i, sat, node, paths = document.querySelector("#paths");
	var od = 5,
		oy = 5,
		ox = 5;

	filter();
	var dump = common_a_b(sats, sats_old);
	if(dump[2].size > 0) {
		console.log(`${jsob.time} -[${Array.from(dump[2])}]`)
		dump[2].forEach(function (e) {
			remove(document.querySelector("#p" + e));
			remove(document.querySelector("#s" + e));
		});
	}
	var r=17;
	if(dump[1].size > 0) {
		console.log(`${jsob.time} +[${Array.from(dump[1]).join(", ")}]`)
		dump[1].forEach(function (e) {
			paths.appendChild(jqmlsvg(["path", {
				"id": `p${e}`
			}]));

			spots.appendChild(jqmlsvg(["g", {
					"id": "s" + e
				},
				["rect"],
				["circle", {
					"cx": 0,
					"cy": 0
				}],
				["polyline", {
                                        "class": "qz", // octagonal
					"points": [22,67,112,157,202,247,292,337,22].map(function(e){var o=polar_to_cart(e,0,17);return `${o.x},${o.y}`}).join(" ")
				}],
                               ["polyline", { // pentagonal
                                        "class": "xx",
                                        "points": [18,90,162,234,306,18].map(function(e){var o=polar_to_cart(e,0,17);return `${o.x},${o.y}`}).join(" ")
                                }],
                               ["polyline", { // hexagonal
                                        "class": "gn",
                                        "points": [30,90,150,210,270,330,30].map(function(e){var o=polar_to_cart(e,0,17);return `${o.x},${o.y}`}).join(" ")
                                }],
				["text", {
					"x": 0,
					"y": 0
				}, e.split("_")[1]]
			]));
		});
	}
	for (i in back_store.sky.satellites) {
		sat = back_store.sky.satellites[i];
		var id = new_id_from_sat(sat);
		if(! sats.includes(id)) {
			break;
		}
		stile = docolor(sat.ss)
		// erygb ft q i
		if ((sat.el <= horizon) | (sat.el > 90) | (sat.az < 0) | (sat.az >= 360)) {
			stile += " i";
		} else {
			if ((sat.gnssid < 7) && (sat.gnssid >= 0)) {
				stile += " " + ["gp", "sb", "ga", "bd", "im", "qz", "gl"][sat.gnssid];
			} else {
				stile += is_defined(sat.PRN) ? " ob" : " xx";
			}
		}
		if (sat.used) {
			stile += " t";
		} else {
			stile += " f";
		}
		var t =  polar_to_cart(sat.az, sat.el, scalar);
		jqmlsvg([document.querySelector(`#s${new_id_from_sat(sat)}`), {
			"class": stile,
			"transform": `translate(${t.x},${t.y})`
		}]);
		var that = sat.PRN, do_it = true;
		if (tracks_old.hasOwnProperty(that)) {
			tracks[that] = tracks_old[that];
			if((tracks[that][0][0] == sat.el) && (tracks[that][0][1] == sat.az)) {
				do_it = false;
			}
		} else {
			tracks[that] = [];
		}
		if (do_it) {
			tracks[that].unshift([sat.el, sat.az]);
		}
		node = document.querySelector(`#p${new_id_from_sat(sat)}`);
		if (is_defined(node)) {
			node.setAttribute("class", stile);
			var linear = svgPath_d(tracks[that])
			node.setAttribute("d", linear);
		}

	}
}

function center_text_spot_resize() {
	var bb, list, len, off, point, wx, wy;
	list = document.querySelectorAll("#spots text");
	len = list.length;
	for (point = 0; len > point; point++) {
		bb = list[point].getBBox();
		wx = bb.width / 2;
		wy = bb.height / 2;
		dx = Math.max(dx, wx);
		dy = Math.max(dy, wy);
		off = list[point].getAttribute("y");
		off = parseFloat(off)
		list[point].y.basevalue = wy / 2;
		list[point].setAttribute("x", -wx);
		list[point].setAttribute("y", wy / 2);
	}

	var node, r = Math.hypot(dx, dy);
	r = Math.ceil(r) - 2;
	list = document.querySelectorAll("#spots circle");
	for (len = list.length - 1; len >= 0; len--) {
		list[len].setAttribute("r", r);
	}
	list = document.querySelectorAll("#spots rect");
	for (len = list.length - 1; len >= 0; len--) {
		node = list[len];
		node.setAttribute("x", -(dx + 1));
		node.setAttribute("y", -(dy + 1));
		node.setAttribute("width", (2 + dx * 2));
		node.setAttribute("height", (2 + dy * 2));
	}
}

function docolor(num) {
	if (30 > num) {
		if (10 > num) {
			return "e";
		} else {
			return "r";
		}
	} else {
		if (35 > num) {
			return "y";
		} else {
			if (40 > num) {
				return "g";
			} else {
				return "b";
			}
		}
	}
}

function common_a_b(a, b) {
	var common = new Set([]);
	var setA = new Set(a);
	var setB = new Set(b);
	a.forEach(function (e) {
		if (setB.has(e)) {
			setB.delete(e);
			setA.delete(e);
			common.add(e)
		}
	});
	return [common, setA, setB];
}

function polar_to_cart(az, el, scale) {
	var off, tx, ty;
	off = (90 - el) * scale / (90 - horizon);

	tx = (off * Math.sin(degrees_to_radians(az))).toFixed(2);
	ty = (-off * Math.cos(degrees_to_radians(az))).toFixed(2);

	return {
		x: tx,
		y: ty
	};
}

function paint_celes() {
	sky_view = [sky_view[0], sky_view[1], []];
	var pos, tr, then = Date.parse(back_store.tpv.time);
	pos = SunCalc.getPosition(then, back_store.tpv.lat, back_store.tpv.lon);
	tr = document.querySelector("#tcel_s");
	tr.cells[4].innerText = radians_to_degrees(pos.altitude).toFixed(1);
	tr.cells[5].innerText = radians_to_degrees(pos.azimuth).toFixed(1);
	if (radians_to_degrees(pos.altitude) >= horizon) {
		pos = polar_to_cart(radians_to_degrees(Math.PI + pos.azimuth),
			radians_to_degrees(pos.altitude),
			scalar);
		jqml([document.querySelector("#scel_s"), {
			"style": " ",
			"transform": `translate(${pos.x},${pos.y})`
		}]);
	} else {
		jqml([document.querySelector("#scel_s"), {
			"style": "visibility:hidden;"
		}])
	}
	pos = SunCalc.getMoonPosition(then, back_store.tpv.lat, back_store.tpv.lon);
	tr = document.querySelector("#tcel_m");
	tr.cells[4].innerText = radians_to_degrees(pos.altitude).toFixed(1);
	tr.cells[5].innerText = radians_to_degrees(pos.azimuth).toFixed(1);
	if (radians_to_degrees(pos.altitude) >= horizon) {
		pos = polar_to_cart(radians_to_degrees(Math.PI + pos.azimuth),
			radians_to_degrees(pos.altitude),
			scalar);
		jqml([document.querySelector("#scel_m"), {
			"style": "",
			"transform": `translate(${pos.x},${pos.y})`
		}]);
	} else {
		jqml([document.querySelector("#scel_m"), {
			"style": "visibility:hidden;"
		}])
	}
	pos = SunCalc.getMoonIllumination(then);
	shadow_moon((pos.phase * 360)%360);
}

function shadow_moon(phase) {
	const sun_scalar = 393;
	var fstop, fstart;

	switch (Math.floor(phase / 90)) {
		case 2:
		case 3:
			fstart = 360 - phase;
			fstop = 0;
			break;
		case 0:
		case 1:
			fstart = 0;
			fstop = phase;
			break;
	}

	var dark_start = (sun_scalar * Math.cos(degrees_to_radians(fstart))).toFixed(2);
	var dark_stop = (sun_scalar * Math.cos(degrees_to_radians(fstop))).toFixed(2);

	var path = `A ${dark_start},${sun_scalar} 0,0,${ dark_start<0 ? 0 : 1 } 0,${-sun_scalar} `;
	path += `A ${dark_stop },${sun_scalar} 0,0,${ dark_stop <0 ? 0 : 1 } 0,${sun_scalar } `;
	path = `M 0,${sun_scalar} ${path} Z`;

	document.querySelector("#dark").setAttribute("d", path);
}

function digits(rational, digits) {
	return Math.round(rational * 10 ** digits) / (10 ** digits)
}

function svgPath_d(points) {
	return "M"+points.map( x => { var p = polar_to_cart(x[1], x[0], scalar); return `${p.x},${p.y}`;}).join("L");
}

function stylize(e) {
		switch(e.target.textContent) {
			case "Degree":
				latlon_format = 1;
				local_store("latlon", 1);
				break;
			case "Minute":
				latlon_format = 2;
				local_store("latlon", 2);
				break;
			case "Second":
				latlon_format = 3;
				local_store("latlon", 3);
				break;
			case "1":
			case "2":
			case "3":
			case "4":
			case "5":
			case "6":
				scopebands = parseInt(e.target.textContent);
				local_store("scopebands", e.target.textContent);
				paint_scope();
				break;
			case "Gem":
			case "Dark":
			case "Woody":
				local_store("style", e.target.textContent);
				jqml([document.body, {"class": e.target.textContent}]);
				break;
			case "Imperial":
			case "Metric":
			case "Nautical":
				local_store("units", e.target.textContent);
				local_config("hgps_units", e.target.textContent);
				paint_tpv();
				break;
		}
}

function stylize2(e) {
	switch(e.target.name) {
		case "url_pull":
			local_store("url", document.querySelector("[name=url]").value)
			events();
			e.target.checked = url_pulling;
			break;
		case "ring_entries":
			local_store(e.target.name, e.target.value);
			ring_entries = e.target.value;
			break;
		case "ring_update":
			local_store(e.target.name, e.target.checked);
			ring_update = e.target.checked;
			break;
		case "horizon":
			horizon = e.target.value;
			local_store(e.target.name, horizon);
			paint_scope();
			paint_sky1();
			paint_sky2();
			break;
		case "json":
			local_store(e.target.name, e.target.checked);
			if (e.target.checked) {
				document.querySelector("#sentence").style.display = "inherit";
			} else {
				document.querySelector("#sentence").style.display = "none";
			}
			break;
		case "skyview":
			local_store(e.target.name, e.target.checked);
			if (e.target.checked) {
				document.querySelector("#skyview").style.display = "inherit";
				document.querySelector("#tabsky").style.display = "inherit";
				center_text_spot_resize();
			} else {
				document.querySelector("#skyview").style.display = "none";
				document.querySelector("#tabsky").style.display = "none";
			}
			break;

	}
}

function is_defined(tested) {
	return (typeof(tested) != "undefined");
}

function is_undefined(tested) {
	return (typeof(tested) == "undefined");
}

function new_id_from_sat(sat) {
	var definitive = 0;
	if (is_undefined(sat.svid)) {
		definitive |= 1<<0;
	}
	if (is_undefined(sat.gnssid)) {
		definitive |= 1<<1;
	}
	if (is_undefined(sat.PRN)) {
		definitive |= 1<<2;
	}
	switch(definitive) {
		case 0:
			definitive = `${sat.gnssid}_${sat.svid}`;
			break;
		case 3:
			definitive = `ob_${sat.PRN}`;
			break;
		default:
			console.log("unsupported satellite configuration:\t"+definitive)
			definitive = "xx_-1";
	}
	return definitive;
}

function filter() {
	sats_old = sats;
	sats = [];

	if (is_defined(back_store.sky.satellites)) {
		back_store.sky.satellites.forEach(function(sat){
			if(sat.el >= horizon) {
				sats.push(sat);
			}
		});
	}
	sats = sats.map(x => new_id_from_sat(x));
}

function sort() {
	var out = [];

	if (is_defined(back_store.sky.satellites)) {
		back_store.sky.satellites.forEach(function(sat){
			if(sat.el >= horizon) {
				out.push(sat);
			}
		});
	} else {
		return false;
	}

	sort_order.forEach(function(key){
		var new_key, rev = ("-" === key[0]);
		new_key = rev ? key.substr(1) : key;
		if (rev) {
			out = out.sort(function(a,b){return b[new_key]-a[new_key];})
		} else {
			out = out.sort(function(a,b){return a[new_key]-b[new_key];})
		}
	});

	out = out.map(x => new_id_from_sat(x));

	out.forEach(function (key){
		prn.appendChild(prn.removeChild(document.querySelector(`#t${key}`)));
	});
}

function resort(e) {
	var txt = e.target.textContent,
		ixlate={"Type": "gnssid", "Num": "svid", "SNR": "ss", "Used": "used", "Elev": "el", "Azim": "az"};
	if(is_defined(ixlate[txt])) {
		var max = sort_order.length - 1;
		txt = ixlate[txt];
		if((sort_order[max] == txt) || (sort_order[max] == `-${txt}`)) {
			var new_rev = ("-" != sort_order[max][0]);
			sort_order[max] = new_rev ? `-${txt}` : txt;
		} else {
			var new_order = [];
			sort_order.forEach(function(key){
				if((sort_order[max] != txt) && (sort_order[max] != `-${txt}`)) {
					new_order.push(key);
				}
			});
			new_order.push(txt);
			sort_order = new_order;
		}
		sort();
	}
}

function local_store(key, value) {
	var new_key = `hgps_${key}`;
	if(typeof(Storage)!=="undefined") {
		localStorage.setItem(new_key,value);
	} else {
		document.cookie=`${new_key}=${value}`;
	}
}

function local_fetch() {
	var count, x=[];
	if(typeof(Storage)!=="undefined") {
		for(count = localStorage.length - 1; count >= 0; count--) {
			x[0] = localStorage.key(count);
			x[1] = localStorage.getItem(x[0]);
			local_config(x[0], x[1]);
		}
	} else {
		var ARRcookies=document.cookie.split("; ");
		for (count = 0; count < ARRcookies.length ; count++ ) {
			x=ARRcookies.split("=");
			local_config(x[0], JSON.parse(x[1]));
		}
	}
}

function local_config(key, value) {
	var new_value;
	switch(key) {
		case "hgps_url":
			url = value;
			document.querySelector("[name=url]").value = url;
			events();
			break;
		case "hgps_horizon":
			new_value = JSON.parse(value);
			document.querySelector("[name=horizon]").value = new_value;
			horizon = new_value
			paint_scope();
			break;
		case "hgps_json":
			new_value = JSON.parse(value);
			document.querySelector("[name=json]").checked = new_value;
			document.querySelector("#sentence").style.display = new_value ? "inherit" : "none";
			break;
		case "hgps_latlon":
			new_value = JSON.parse(value);
			latlon_format = new_value;
			break;
		case "hgps_ring_entries":
			new_value = JSON.parse(value);
			document.querySelector("[name=ring_entries]").value = new_value;
			ring_entries = new_value;
			break;
		case "hgps_ring_update":
			new_value = JSON.parse(value);
			document.querySelector("[name=ring_update]").checked = new_value;
			ring_update = new_value;
			break;
		case "hgps_scopebands":
			new_value = JSON.parse(value);
			scopebands = new_value;
			paint_scope();
			break;
		case "hgps_skyview":
			new_value = JSON.parse(value);
			document.querySelector("[name=skyview]").checked = new_value;
			document.querySelector("#skyview").style.display = new_value ? "inherit" : "none";
			document.querySelector("#tabsky").style.display = new_value ? "inherit" : "none";
			center_text_spot_resize();
			break;
		case "hgps_style":
			jqml([document.body, {"class": value}]);
			break;
		case "hgps_units":
			latlon_format = value;
			document.body.id = value;
			shim_len  = {"Metric":1, "Imperial":3.2808, "Nautical":3.2808}[value];
			shim_move = {"Metric":1, "Imperial":2.2369, "Nautical":1.9438}[value];
			break;
	}
}

function events() {
	if (url_pulling) {
		url_source.close();
		url_pulling = false;
	} else {
		if (is_defined(EventSource)) {
			url_source = new EventSource(url);
			url_pulling = true;
			document.querySelector("[name=url_pull]").checked = true;
			url_source.onmessage = function (event) {
				if(ring_update) {
					back_store.ring.push(event.data);
					if (ring_entries < back_store.ring.length) {
						back_store.ring = back_store.ring.splice((back_store.ring.length - ring_entries), (back_store.ring.length -1))
					}
					document.querySelector("#sentence").value = back_store.ring.join("\n");
				}
				jsob = JSON.parse(event.data);
				back_store[jsob.class] = jsob;
				repaint(jsob.class);
			};
		} else {
			Alert("No server-sent events support");
		}
	}
}
