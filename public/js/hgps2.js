"use strict";
const SVGns = "http://www.w3.org/2000/svg";
var gap = 20,
    url = "https://dell-2018.jamesb192.com/cgi-bin/gpso.py",
    url_pulling = false,
    url_source,
    sort_order = ["svid", "gnssid", "-used"],
    back_store = {
        "ring": []
    },
    ring_update = true,
    ring_entries = 3,
    horizon = -10,
    scalar = 230,
    sky_view = [
        [],
        [],
        []
    ],
    shim_len = 1,
    shim_move = 1,
    latlon_format = 1,
    from, paths, spots, jsob, back, bod, prn, arrow;
var scopebands = 3,
    dx = 0,
    dy = 0;
var tracks = {},
    tracks_old = {},
    sats = [],
    sats_old = [],
    staled = {},
    debug = {};
// 45 37 32 20 5 // don't you love it when you forget what your notes are for?

function degrees_to_radians(degrees) {
    return degrees / 180 * Math.PI;
}

function empty(grandparent) {
    for (var count = grandparent.childElementCount - 1; count >= 0; count--) {
        grandparent.removeChild(grandparent.childNodes[count]);
    }
}

function remove(child) {
    child.parentElement.removeChild(child);
}

function paint_scope() {
    var d = "",
        i, k, step, t, bb, xy, rm = document.querySelector("#ringmark");
    empty(rm);
    if ((scopebands <= 6) && (scopebands >= 1)) {
        for (step = 0; step <= 90 - horizon; step += 90 / scopebands) {
            i = polar_to_cart(180, step, scalar).y;
            k = `A${i} ${i} 0 0 0 0,`;
            d += `M0,${i}${k}${-i}${k}${i}Z`;
            if (step < 90) {
                xy = polar_to_cart(45, step, scalar);
                var txt = document.createElementNS(SVGns, "text");
                txt.setAttribute("x", xy.x);
                txt.setAttribute("y", xy.y);
                txt.textContent = `${digits(step,1)}°`;
                rm.appendChild(txt);
            }
        }
    }
    i = gap + scalar;
    k = `A${i} ${i} 0 0 0 0,`;
    d += `M0,${i}${k}${-i}${k}${i}Z`;
    d += `M${-i},0H${i}ZM0,${-i}V${i}Z`;

    document.querySelector("#scope").setAttribute("d", d);
    k = [
        [0, "N"],
        [90, "E"],
        [180, "S"],
        [270, "W"]
    ];
    for (d = 0; d < 4; d++) {
        i = k[d];
        xy = polar_to_cart(i[0], 0, scalar);
        var txt = document.createElementNS(SVGns, "text");
        txt.textContent = i[1];
        txt.setAttribute("x", xy.x);
        txt.setAttribute("y", xy.y);
        rm.appendChild(txt);
    }
}

function local_init() {
    let emplate = {
	hgps_horizon: "-1",
	hgps_json: "true",
	hgps_latlon: "3",
	hgps_ring_entries: "4",
	hgps_ring_update: "true",
	hgps_scopebands: "3",
	hgps_skyview: "true",
	hgps_style: "Gem",
	hgps_units: "Metric",
	hgps_url: "https://dell-2018..jamesb192.com/cgi-bin/gpso.py",
	hgps_url_pull: "false"
    };
	for (let key in emplate) {
	    if (!(key in localStorage)) {
		localStorage[key] = emplate[key];
	    }
	}
}

function init() {
    var d, e, i, j, k;
    bod = document.querySelector("body");
    paths = document.querySelector("#paths");
    local_init();
    local_fetch();
    document.querySelector("[name=url]").value = url;

    from = document.querySelector("form");
    arrow = document.querySelector("#arrow");
    prn = document.querySelector("#prn");
    spots = document.querySelector("#spots");

    document.querySelector("#tabsky thead").addEventListener("click", function (e) {
        resort(e)
    });
    document.querySelector("#navbar").addEventListener("input", function (e) {
        stylize2(e)
    });
    document.querySelector("#navbar").addEventListener("click", function (e) {
        stylize(e)
    });
    spots.addEventListener("mouseout", function (e) {
        document.querySelector("#tabtip").setAttribute("style", " ");
    });
    spots.addEventListener("mouseover", function (e) {
        var id = e.target.textContent;
        if (id != "") {
            var table = prn;
            for (var nrow = table.rows.length - 1; nrow >= 0; nrow--) {
                var row = table.rows[nrow];
                if ((row.cells[1].innerHTML.toString() == id) && (e.target.parentNode.classList.value.includes(row.cells[0].innerHTML.toLowerCase()))) {
                    paint_tip(e, row);
                    return;
                }
            }
        }
    });
}

function paint_tip(e, row) {
    var tmp, HTML = e.target.innerHTML;
    tmp = `${row.cells[0].innerHTML} ${row.cells[1].innerHTML}`;
    document.querySelector("#tip0").innerHTML = tmp;
    document.querySelector("#tip4").innerHTML = row.cells[4].innerHTML;
    document.querySelector("#tip5").innerHTML = row.cells[5].innerHTML;
    var lefty = e.pageX - document.querySelector("#tabtip").clientWidth - 45;
    var toppy = e.pageY - document.querySelector("#tabtip").clientHeight / 2 - 9;
    var styled = "visibility:visible;top:" + toppy + "px;left:" + lefty + "px;";
    document.querySelector("#tabtip").setAttribute("style", styled);
    return;
}

function repaint(myclass) {
    if (is_defined(myclass)) {
        switch (myclass) {
        case "tpv":
            paint_tpv();
            doEP();
            break;
        case "sky":
            tracks_old = tracks;
            jsob.satellites.forEach(function (sat) {
                var id = sat.PRN;
                if (is_defined(staled[id])) {
                    staled[id] -= 1;
                } else {
                    staled[id] = 0;
                }
            });
            Object.keys(tracks_old).forEach(function (e) {
                if (is_undefined(staled[e])) {
                    staled[e] = 1;
                } else {
                    staled[e] += 1;
                }
                if (staled[e] > 10) {
                    delete staled[e];
                    delete tracks[e];
                }
            });
            doDOP();
            paint_sky1();
            paint_sky2();
            center_text_spot_resize();
            break;
        case "TOFF":
        case "PPS":
            var diff = back_store[myclass].real_sec - back_store[myclass].clock_sec;
            if (is_undefined(back_store[myclass].clock_nsec)) {
                diff += (back_store[myclass].real_musec - back_store[myclass].clock_musec) / 1e6;
                diff = diff.toFixed(6)
            } else {
                diff += (back_store[myclass].real_nsec - back_store[myclass].clock_nsec) / 1e9;
                diff = diff.toFixed(9)
            }
            from[myclass].value = diff;
            break;
        case "VERSION":
            var fields = ["proto_major", "proto_minor", "release", "rev"];
            fields.forEach(function (field) {
                if (is_defined(back_store.VERSION[field])) {
                    from[`version_${field}`].value = back_store.VERSION[field];
                } else {
                    from[`version_${field}`].value = "";
                }
            });
            break;
        case "DEVICES":
            var fields = ["path", "driver", "subtype"];
            fields.forEach(function (field) {
                if (is_defined(back_store.DEVICES.devices[0][field])) {
                    from[`device_${field}`].value = back_store.DEVICES.devices[0][field];
                } else {
                    from[`device_${field}`].value = "";
                }
            });
            break;
        case "DEVICE":
            var fields = ["driver", "subtype"];
            fields.forEach(function (field) {
                if (is_defined(back_store.DEVICE[field])) {
                    from[`device_${field}`].value = back_store.DEVICE[field];
                } else {
                    from[`device_${field}`].value = "";
                }
            });
            break;
        case "inital_tracking":
            tracks = back_store.inital_tracking.points;
            Object.keys(tracks).forEach(function (e) { // Filter out silly data points
                tracks[e] = tracks[e].filter(function (f) {
                    return ((f[0] >= -20) && (f[0] <= 90) && (f[1] >= 0) && (f[1] < 360));
                });
            });
            back_store.tracks = tracks;
            break;
        }
    }
}

function paint_tpv() {
    var d, e, m, s, z, ref, list = ["lat", "lon", "track", "epd"],
        dowp = ["Sun", "Mon", "Tues", "Wednes", "Thurs", "Fri", "Satur"];

    d = new Date(back_store.tpv.time);
    e = d.toString().split(" ");
    //	f = Intl.DateTimeFormat.prototype.formatToParts(d)

    from["time"].value = e[4] + (d.getHours() < 12 ? " AM" : " PM");
    from["date"].value = `${dowp[d.getDay()]}day ${e[1]} ${e[2]} ${e[3]}`;

    if (("number" == typeof (back_store.tpv.track)) && ("number" == typeof (back_store.tpv.magtrack))) {
        d = 0;
        // d = back_store.tpv.track - back_store.tpv.magtrack;
        // d = back_store.tpv.magtrack;
    } else {
        d = 0;
    }
    s = document.querySelector("#css").sheet.cssRules;
    s[0].style.transform = `rotate(${-d}deg)`;
    s[1].style.transform = `rotate(${d}deg)`;
    for (ref in list) {
        if (is_defined(back_store.tpv[list[ref]])) {
            d = back_store.tpv[list[ref]];
            switch (latlon_format) {
            case 2:
                m = (d % 1) * 60;
                d = Math.floor(d);
                if (d < 0) {
                    d += 1;
                }
                if (m < 0) {
                    m = -m;
                }
                from[list[ref]].value = `${d}°${m.toFixed(2)}\'`;
                break;
            case 3:
                m = (d % 1) * 60;
                d = Math.floor(d);
                s = (m % 1) * 60;
                m = Math.floor(m);

                if (d < 0) {
                    d += 1;
                }
                if (m < 0) {
                    m = -1 - m;
                }
                if (s < 0) {
                    s = -s;
                }
                from[list[ref]].value = `${d}°${m}\'${s.toFixed(2)}\"`;
                break;
            default:
                from[list[ref]].value = `${d}°`;
            }
        }
    }
    var ref, list = ["altHAE", "altMSL", "epx", "epy", "epv", "ecefx", "ecefy", "ecefz", "ecefpAcc"]
    for (ref in list) {
        if (is_defined(back_store.tpv[list[ref]])) {
            from[list[ref]].value = (back_store.tpv[list[ref]] * shim_len).toFixed(2);
        }
    }
    var ref, list = ["speed", "climb", "eps", "epc", "ecefvx", "ecefvy", "ecefvz", "ecefvAcc"]
    for (ref in list) {
        if (is_defined(back_store.tpv[list[ref]])) {
            from[list[ref]].value = (back_store.tpv[list[ref]] * shim_move).toFixed(2);
        }
    }
    from.tzone.value = Intl.DateTimeFormat().resolvedOptions().timeZone;
    if (is_defined(back_store.tpv.track)) {
        if (is_defined(back_store.tpv.epd)) {
            ref = back_store.tpv.epd;
        } else {
            ref = 0;
        }
        if(isFinite(ref)&&isFinite(horizon)) {
            ref = polar_to_cart(ref, 0, 84.85);
            ref = `M0,0L${-ref.x},${ref.y}A84.85,84.85 0,0,1 ${ref.x},${ref.y}z`
            arrow.children[0].setAttribute("d", ref);
        }
        ref = 30 / (back_store.tpv.speed + Math.abs(back_store.tpv.eps));
        if (!isFinite(ref)) {
            ref = 0;
        }
        arrow.children[1].setAttribute("d", `M0,-84.85L0,${-84.85 - (ref * back_store.tpv.speed)}`);
        arrow.children[2].setAttribute("d", `M0,-84.85L0,${-54.85 - (ref * back_store.tpv.speed)}`);
        arrow.setAttribute("transform", `rotate(${back_store.tpv.track})`);
    }
    if ((back_store.tpv.mode >= 0) && (back_store.tpv.mode < 4)) {
        from['tpv_mode'].value = ["not yet fixed", "no fix", "2d fix", "3d fix"][back_store.tpv.mode];
    } else {
        from['tpv_mode'].value = ""
    }
    if (is_defined(back_store.tpv.status)) {
        from['tpv_status'].value = back_store.tpv.status;
    } else {
        from['tpv_status'].value = "";
    }
}

function paint_sky2() {
    sky_view = [
        [],
        [], sky_view[2]
    ]
    var dump = common_a_b(sats, sats_old);
    dump[2].forEach(function (e) {
        // Mark expiring entries for removal
        document.querySelector("#t" + e).setAttribute("class", "d");
    });
    var my_dict = tr_struct_from_sat()
    dump[1].forEach(function (e) {
        // Add the filled table row
        var tr = prn.insertRow(-1);
        tr.setAttribute("class", "a");
        tr.setAttribute("id", `t${e}`);
        for (var count = 5; count >= 0; count--) {
            var td = tr.insertCell(0);
            td.innerText = my_dict[e][count];
        }
    });
    dump[0].forEach(function (e) {
        // Update the filled table row
        var tr, my_arr = my_dict[e];
        tr = document.querySelector(`#t${e}`);
        for (var count = 5; count >= 0; count--) {
            if (tr.cells[count].innerText != my_dict[e][count]) {
                tr.cells[count].innerText = my_dict[e][count];
                tr.cells[count].setAttribute("class", "u");
            }
        }
    });

    sort();
    var used = back_store.sky.satellites.map(
                        function(e){return e.used;}
                    ).reduce(
                        function(a,b){return a+b;}
                    );
    document.querySelector("#seen").innerText = `${used} / ${sats.length}`;

    setTimeout(function () {
        dump[2].forEach(function (e) {
            remove(document.querySelector(`#t${e}`));
        });
        document.querySelectorAll("tr.a").forEach(function (node) {
            node.classList.remove("a");
        });
        document.querySelectorAll("td.u").forEach(function (node) {
            node.classList.remove("u");
        });
    }, 701);
}

function tr_struct_from_sat() {
    var inner, key, ta = {},
        tr;
    back_store.sky.satellites.forEach(function (sat) {

        key = `t${new_id_from_sat(sat)}`;
        tr = [];
        ["gnssid", "svid", "ss", "used", "el", "az"].forEach(function (iter) {
            key = new_id_from_sat(sat)
            switch (iter) {
            case "gnssid":
                inner = "UN";
                if (is_undefined(sat.gnssid)) {
                    inner = "OB";
                } else if ((sat.gnssid < 7) && (sat.gnssid >= 0)) {
                    inner = ["GP", "SB", "GA", "BD", "IM", "QZ", "GL"][sat.gnssid];
                }
                break;
            case "used":
                inner = sat[iter] ? "√" : "";
                break;
            case "svid":
                inner = is_defined(sat.PRN) ? sat.PRN : "-1";
                inner = is_defined(sat[iter]) ? sat.svid : inner;
                break;
            default:
                inner = is_defined(sat[iter]) ? sat[iter] : "";
            }
            tr.push(inner);
        });
        ta[key] = tr;
    });
    return ta;
}

function doDOP() {
    //  DOPs x y v t h g p
    var list = ["xdop", "ydop", "vdop", "tdop", "hdop", "gdop", "pdop"];
    for (var ref in list) {
        if (is_defined(back_store.sky[list[ref]])) {
            from[list[ref]].value = back_store.sky[list[ref]].toFixed(2);
        } else {
            from[list[ref]].value = "";
        }
    }
}

function doEP() {
    var list = ["epx", "epy", "epe", "epv", "eps", "epc", "ept"];
    for (var ref in list) {
        name = "t_" + list[ref];
        if (is_defined(back_store.tpv[list[ref]])) {
            from[name].value = back_store.tpv[list[ref]].toFixed(2);
        } else {
            from[name].value = " ";
        }
    }
}

function paint_sky1() {
    var stile, i, sat, node;
    var od = 5,
        oy = 5,
        ox = 5;

    filter();
    var dump = common_a_b(sats, sats_old);
    if (dump[2].size > 0) { // remove
        console.log(`${back_store.tpv.time} -[${Array.from(dump[2])}]`)
        dump[2].forEach(function (e) {
            remove(document.querySelector("#p" + e));
            remove(document.querySelector("#s" + e));
        });
    }
    var r = 17;
    if (dump[1].size > 0) { // add
        console.log(`${back_store.tpv.time} +[${Array.from(dump[1]).join(", ")}]`)
        dump[1].forEach(function (e) {
            var path = document.createElementNS(SVGns, "path");
            path.setAttribute("id", `p${e}`);
            paths.appendChild(path);
            // var my_use = document.createElementNS(SVGns, "use");
            var my_g = document.createElementNS(SVGns, "g");
            var my_text = document.createElementNS(SVGns, "text");
            // my_use.setAttributeNS(SVGns, "href", "#spot_back");
            my_g.setAttribute("id", `s${e}`);
            my_text.textContent = e.split("_")[1];
            var my_node, bucket = e.split("_")[0];
            if (bucket == "0") { // GPS get circles
                my_node = document.createElementNS(SVGns, "circle");
                my_node.setAttribute("cx", 0);
                my_node.setAttribute("cy", 0);
                my_node.setAttribute("r", 17);
            } else if (bucket == "1") { // SBAS and WAAS get rectangles
                my_node = document.createElementNS(SVGns, "rect");
                my_node.setAttribute("x", -17);
                my_node.setAttribute("y", -17);
                my_node.setAttribute("width", 34);
                my_node.setAttribute("height", 34);
            } else { // Everyone else has a polyline
                my_node = document.createElementNS(SVGns, "polyline");
		var strung = "";
		for (var theta=-0.125;theta < 2;theta += 0.25) {
			strung += 17*Math.cos(theta * Math.PI)+","+17*Math.sin(theta * Math.PI)+" ";
		}
                my_node.setAttribute("points", strung);
            }
            my_g.appendChild(my_node);
            my_g.appendChild(my_text);
            // my_g.appendChild(my_use);
            spots.appendChild(my_g);
        });
    }
    for (i in back_store.sky.satellites) {
        sat = back_store.sky.satellites[i];
        node = document.querySelector(`#s${new_id_from_sat(sat)}`);
        if (node == null) {
            break;
        }
        /// Does it work?
        stile = [];
        var id = new_id_from_sat(sat);
        if (!sats.includes(id)) {
            break;
        }
        stile.push(docolor(sat.ss));
        // erygb ft q i
        if ((sat.el <= horizon) | (sat.el > 90) | (sat.az < 0) | (sat.az >= 360)) {
            stile.push("i");
        } else {
            if ((sat.gnssid < 7) && (sat.gnssid >= 0)) {
                id = ["gp", "sb", "ga", "bd", "im", "qz", "gl"][sat.gnssid];
                stile.push(id);
            } else {
                id = is_defined(sat.PRN) ? "ob" : "un"
                stile.push(id);
            }
        }
        if (sat.used) {
            stile.push("t");
        } else {
            stile.push("f");
        }
        //              console.info(stile);
        stile = stile.join(" ");
        node.setAttribute("class", stile);
        if (isFinite(sat.az)&&isFinite(sat.el)) {
            var t = polar_to_cart(sat.az, sat.el, scalar);
            node.setAttribute("transform", `translate(${t.x},${t.y})`);
        }
        var that = sat.PRN,
            do_it = true;
        if (tracks_old.hasOwnProperty(that)) {
            tracks[that] = tracks_old[that];
            if ((tracks[that][0][0] == sat.el) && (tracks[that][0][1] == sat.az)) {
                do_it = false;
            }
        } else {
            tracks[that] = [];
        }
        if (do_it) {
            tracks[that].unshift([sat.el, sat.az]);
        }
        node = document.querySelector(`#p${new_id_from_sat(sat)}`);
        if (is_defined(node) && (node != null)) {
            node.setAttribute("class", stile);
            var linear = svgPath_d(tracks[that].filter(function(e){return (isFinite(e[0])&&isFinite(e[1]))}))
            node.setAttribute("d", linear);
        }

    }
    node = document.querySelectorAll("#spots polyline");
    for (that = node.length - 1; that >= 0; that--) {
        var type = node[that].parentElement.className.baseVal.split(" ")[1];
        add_polypoint(node[that], type);
    }
}

function center_text_spot_resize() {
    var bb, list, len, off, point, wx, wy, r;
    list = document.querySelectorAll("#spots text");
    len = list.length;
    for (point = 0; len > point; point++) {
        bb = list[point].getBBox();
        wx = bb.width / 2;
        wy = bb.height / 2;
        dx = Math.max(dx, wx);
        dy = Math.max(dy, wy);
        r = Math.hypot(dx, dy) / 2;
        off = list[point].getAttribute("y");
        off = parseFloat(off)
        //		list[point].y.basevalue = wy;
        list[point].setAttribute("x", -wx);
        list[point].setAttribute("y", wy / 2);
    }

    list = document.querySelectorAll("#spots rect");
    for (point = list.length - 1; point >= 0; point--) {
        list[point].setAttribute("x", -(dx + 2));
        list[point].setAttribute("y", -(dy + 2));
        list[point].setAttribute("width", `${4 + dx * 2}px`);
        list[point].setAttribute("height", `${4 + dy * 2}px`);
    }
    list = document.querySelectorAll("#spots circle");
    bb = Math.ceil(Math.hypot(dx, dy) - 2)
    for (point = list.length - 1; point >= 0; point--) {
        list[point].setAttribute("r", bb);
    }
    list = document.querySelectorAll("#spots polyline");
    bb = Math.ceil(Math.hypot(dx, dy) - 2)
    for (point = list.length - 1; point >= 0; point--) {
        list[point].setAttribute("transform", `scale(${digits(r / 17, 2)})`);
    }
}

function docolor(num) {
    if (30 > num) {
        if (10 > num) {
            return "e";
        } else {
            return "r";
        }
    } else {
        if (35 > num) {
            return "y";
        } else {
            if (40 > num) {
                return "g";
            } else {
                return "b";
            }
        }
    }
}

function common_a_b(a, b) {
    var common = new Set([]);
    var setA = new Set(a);
    var setB = new Set(b);
    a.forEach(function (e) {
        if (setB.has(e)) {
            setB.delete(e);
            setA.delete(e);
            common.add(e)
        }
    });
    return [common, setA, setB];
}

function polar_to_cart(az, el, scale) {
    var off, tx, ty;
    off = (90 - el) * scale / (90 - horizon);

    tx = (off * Math.sin(degrees_to_radians(az))).toFixed(2);
    ty = (-off * Math.cos(degrees_to_radians(az))).toFixed(2);

    if (isFinite(tx) && isFinite(ty)) {
        return {
            x: tx,
            y: ty
        };
    } else {
        console.warn(`p2c(${az}, ${el}, ${scale}) -> ${tx}, ${ty}`);
    }
}

function digits(rational, digits) {
    return Math.round(rational * 10 ** digits) / (10 ** digits)
}

function svgPath_d(points) {
    return "M" + points.filter(function(e){return (isFinite(e[0])&&isFinite(e[1]))}).map(x => {
        var p = polar_to_cart(x[1], x[0], scalar);
        return `${p.x},${p.y}`;
    }).join("L");
}

function stylize(e) {
    switch (e.target.textContent) {
    case "Degree":
        latlon_format = 1;
        local_store("latlon", 1);
        break;
    case "Minute":
        latlon_format = 2;
        local_store("latlon", 2);
        break;
    case "Second":
        latlon_format = 3;
        local_store("latlon", 3);
        break;
    case "1":
    case "2":
    case "3":
    case "4":
    case "5":
    case "6":
        scopebands = parseInt(e.target.textContent);
        local_store("scopebands", e.target.textContent);
        paint_scope();
        break;
    case "Gem":
    case "Dark":
    case "Woody":
        local_store("style", e.target.textContent);
        document.body.setAttribute("class", e.target.textContent);
        break;
    case "Imperial":
    case "Metric":
    case "Nautical":
        local_store("units", e.target.textContent);
        local_config("hgps_units", e.target.textContent);
        paint_tpv();
        break;
    }
}

function stylize2(e) {
    switch (e.target.name) {
    case "url_pull":
        local_store("url", document.querySelector("[name=url]").value)
	url_pulling = !url_pulling;
        e.target.checked = url_pulling;
	events();
        break;
    case "ring_entries":
        local_store(e.target.name, e.target.value);
        ring_entries = e.target.value;
        break;
    case "ring_update":
        local_store(e.target.name, e.target.checked);
        ring_update = e.target.checked;
        break;
    case "horizon":
        horizon = e.target.value;
        local_store(e.target.name, horizon);
        paint_scope();
        paint_sky1();
        paint_sky2();
        break;
    case "json":
        local_store(e.target.name, e.target.checked);
        if (e.target.checked) {
            document.querySelector("#sentence").style.display = "inherit";
        } else {
            document.querySelector("#sentence").style.display = "none";
        }
        break;
    case "skyview":
        local_store(e.target.name, e.target.checked);
        if (e.target.checked) {
            document.querySelector("#skyview").style.display = "inherit";
            document.querySelector("#tabsky").style.display = "inherit";
            center_text_spot_resize();
        } else {
            document.querySelector("#skyview").style.display = "none";
            document.querySelector("#tabsky").style.display = "none";
        }
        break;

    }
}

function is_defined(tested) {
    return (typeof (tested) != "undefined");
}

function is_undefined(tested) {
    return (typeof (tested) == "undefined");
}

function new_id_from_sat(sat) {
    var definitive = 0;
    if (is_undefined(sat.svid)) {
        definitive |= 1 << 0;
    }
    if (is_undefined(sat.gnssid)) {
        definitive |= 1 << 1;
    }
    if (is_undefined(sat.PRN)) {
        definitive |= 1 << 2;
    }
    switch (definitive) {
    case 0:
        definitive = `${sat.gnssid}_${sat.svid}`;
        break;
    case 3:
        definitive = `ob_${sat.PRN}`;
        break;
    default:
        console.log("unsupported satellite configuration:\t" + definitive)
        definitive = "un_-1";
    }
    return definitive;
}

function filter() {
    sats_old = sats;
    sats = [];

    if (is_defined(back_store.sky.satellites)) {
        back_store.sky.satellites.forEach(function (sat) {
            if (sat.el >= horizon) {
                sats.push(sat);
            }
        });
    }
    sats = sats.map(x => new_id_from_sat(x));
}

function sort() {
    var out = [];

    if (is_defined(back_store.sky.satellites)) {
        back_store.sky.satellites.forEach(function (sat) {
            if (sat.el >= horizon) {
                out.push(sat);
            }
        });
    } else {
        return false;
    }

    sort_order.forEach(function (key) {
        var new_key, rev = ("-" === key[0]);
        new_key = rev ? key.substr(1) : key;
        if (rev) {
            out = out.sort(function (a, b) {
                return b[new_key] - a[new_key];
            })
        } else {
            out = out.sort(function (a, b) {
                return a[new_key] - b[new_key];
            })
        }
    });

    out = out.map(x => new_id_from_sat(x));

    out.forEach(function (key) {
        prn.appendChild(prn.removeChild(document.querySelector(`#t${key}`)));
    });
    out.reverse().forEach(function (key) {
        spots.appendChild(spots.removeChild(document.querySelector(`#s${key}`)));
        paths.appendChild(paths.removeChild(document.querySelector(`#p${key}`)));
    });
}

function resort(e) {
    var txt = e.target.textContent,
        ixlate = {
            "Type": "gnssid",
            "Num": "svid",
            "SNR": "ss",
            "Used": "used",
            "Elev": "el",
            "Azim": "az"
        };
    if (is_defined(ixlate[txt])) {
        var max = sort_order.length - 1;
        txt = ixlate[txt];
        if ((sort_order[max] == txt) || (sort_order[max] == `-${txt}`)) {
            var new_rev = ("-" != sort_order[max][0]);
            sort_order[max] = new_rev ? `-${txt}` : txt;
        } else {
            var new_order = [];
            sort_order.forEach(function (key) {
                if ((sort_order[max] != txt) && (sort_order[max] != `-${txt}`)) {
                    new_order.push(key);
                }
            });
            new_order.push(txt);
            sort_order = new_order;
        }
        sort();
    }
}

function local_store(key, value) {
    var new_key = `hgps_${key}`;
    if (typeof (Storage) !== "undefined") {
        localStorage.setItem(new_key, value);
    } else {
        document.cookie = `${new_key}=${value}`;
    }
}

function local_fetch() {
    var count, x = [];
    if (typeof (Storage) !== "undefined") {
        for (count = localStorage.length - 1; count >= 0; count--) {
            x[0] = localStorage.key(count);
            x[1] = localStorage.getItem(x[0]);
            local_config(x[0], x[1]);
        }
    } else {
        var ARRcookies = document.cookie.split("; ");
        for (count = 0; count < ARRcookies.length; count++) {
            x = ARRcookies.split("=");
            local_config(x[0], JSON.parse(x[1]));
        }
    }
}

function local_config(key, value) {
    var new_value;
    switch (key) {
    case "hgps_url":
        url = value;
        document.querySelector("[name=url]").value = url;
        break;
    case "hgps_horizon":
        new_value = JSON.parse(value);
        document.querySelector("[name=horizon]").value = new_value;
        horizon = new_value
        paint_scope();
        break;
    case "hgps_json":
        new_value = JSON.parse(value);
        document.querySelector("[name=json]").checked = new_value;
        document.querySelector("#sentence").style.display = new_value ? "inherit" : "none";
        break;
    case "hgps_latlon":
        new_value = JSON.parse(value);
        latlon_format = new_value;
        break;
    case "hgps_ring_entries":
        new_value = JSON.parse(value);
        document.querySelector("[name=ring_entries]").value = new_value;
        ring_entries = new_value;
        break;
    case "hgps_ring_update":
        new_value = JSON.parse(value);
        document.querySelector("[name=ring_update]").checked = new_value;
        ring_update = new_value;
        break;
    case "hgps_scopebands":
        new_value = JSON.parse(value);
        scopebands = new_value;
        paint_scope();
        break;
    case "hgps_url_pull":
        url_pulling = JSON.parse(value);
        document.querySelector("[name=url_pull]").checked = url_pulling;
	if (url_pulling) {
	    events();
	}
        break;
    case "hgps_skyview":
        new_value = JSON.parse(value);
        document.querySelector("[name=skyview]").checked = new_value;
        document.querySelector("#skyview").style.display = new_value ? "inherit" : "none";
        document.querySelector("#tabsky").style.display = new_value ? "inherit" : "none";
        center_text_spot_resize();
        break;
    case "hgps_style":
        document.body.class = value;
        break;
    case "hgps_units":
        latlon_format = value;
        document.body.id = value;
        shim_len = {
            "Metric": 1,
            "Imperial": 3.2808,
            "Nautical": 3.2808
        } [value];
        shim_move = {
            "Metric": 1,
            "Imperial": 2.2369,
            "Nautical": 1.9438
        } [value];
        break;
    }
}

function events() {
    if (!url_pulling) {
        url_source.close();
    } else {
        if (is_defined(EventSource)) {
            url_source = new EventSource(url);
            url_pulling = true;
            document.querySelector("[name=url_pull]").checked = true;
            url_source.onmessage = function (event) {
                if (ring_update) {
                    back_store.ring.push(event.data);
                    if (ring_entries < back_store.ring.length) {
                        back_store.ring = back_store.ring.splice((back_store.ring.length - ring_entries), (back_store.ring.length - 1))
                    }
                    document.querySelector("#sentence").value = back_store.ring.join("\n");
                }
                jsob = JSON.parse(event.data);
                back_store[jsob.class] = jsob;
                repaint(jsob.class);
            };
        } else {
            Alert("No server-sent events support");
        }
    }
}

function add_polypoint(n, bucket) {
    var c, p, r;
    // "GA", "BD", "IM", "QZ", "GL"
    p = {
        "qz": [0,90,180,270,0],                 // diamond
        "ob": [30, 90, 150, 210, 270, 330, 30], // hexagonal -
        "xx": [0, 60, 120, 180, 240, 300, 0],   // hexagonal |
        "gn": [18, 90, 162, 234, 306, 18],      // pentagonal >
        "bd": [0, 72, 144, 216, 288, 0],        // pentagon ^
        "im": [54, 342, 270, 198, 126, 54],     // pentagon <
        "ga": [36, 108, 180, 252, 324, 36],     // pentagon v
    };
    if ((n == null)||(Object.keys(p).indexOf(bucket))>=0) {
        return false;
    }
    r = p[bucket].map(function (e) {
        var o = polar_to_cart(e, horizon, 17);
        return `${o.x},${o.y}`
    }).join(" ");
    n.setAttribute("points", r);
    n.setAttribute("class", bucket);
    return true;
}
