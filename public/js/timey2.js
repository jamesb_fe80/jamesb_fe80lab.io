"use strict";
let host = "Aardvark.local";
let tFormat = "Y-MM-DD HH:mm:ss";
let d3_date;
let d3_date2;

let jdata = {};

let gpsPlot = [];
let tempfreqPlot = [];
let loffsetPlot =[];
let peersoffsetPlot = [];
let peersjitterPlot = [];
let ljitterPlot = [];
let histoPlot = [];

let layoutClockFreq = {title: host+": Local Clock/Frequency Offset",showLegend:false,yaxis:{"showgrid": true, "gridcolor": "rgba(0,0,0,0.5)", title:"s",fixedrange:true},yaxis2:{"showgrid": true, "gridcolor": "rgba(0,0,0,0.5)", title:"ppm",overlaying:"y",side:"right",fixedrange:true}};
let layoutTempFreq = {title: host+": Local Frequency Offset/Temps",showLegend:false,yaxis:{"showgrid": true, "gridcolor": "rgba(0,0,0,0.5)", title:"&deg;C",fixedrange:true},yaxis2:{"showgrid": true, "gridcolor": "rgba(0,0,0,0.5)", title:"ppm",overlaying:"y",side:"right",fixedrange:true}};
let layoutGPS  = {title: host+": Local GPS",yaxis:{"showgrid": true, "gridcolor": "rgba(0,0,0,0.5)", title:"TDOP",fixedrange:true},yaxis2:{"showgrid": true, "gridcolor": "rgba(0,0,0,0.5)", title:"nSat",overlaying:"y",side:"right",fixedrange:true}};
let layoutJitters  = {title: host+": Peer Jitters",yaxis:{"showgrid": true, "gridcolor": "rgba(0,0,0,0.5)", title:"ms",fixedrange:true},overlaying:"y",side:"left",fixedrange:true};
let layoutOffsets  = {title: host+": Peer Offsets",yaxis:{"showgrid": true, "gridcolor": "rgba(0,0,0,0.5)", title:"ms",fixedrange:true},overlaying:"y",side:"left",fixedrange:true};
let layoutLJitters = {title: host+": Local Clock Time/Frequency Jitters",yaxis:{"showgrid": true, "gridcolor": "rgba(0,0,0,0.5)", title:"ms",fixedrange:true},yaxis:{"showgrid": true, "gridcolor": "rgba(0,0,0,0.5)", title:"ppm",fixedrange:true,side:"right"},overlaying:"y",side:"left",fixedrange:true};
let layout_histo = {"title": host+": Local Clock Time Offset Histogram"};

let selectorOptions = {
    buttons: [{
        step: "day",
        stepmode: "backward",
        count: 1,
        label: "1d"
    }, {
        step: "week",
        stepmode: "backward",
        count: 1,
        label: "1w"
    }, {
        step: "all",
    }],
};

function date(tai){
	return d3_date(tai*1000);
  }
function date2(tai){
	return d3_date2(tai*1000);
  }

async function gpsgraph(){
	var tdop = [];
	var nsat = [];
	var keylist = Object.keys(jdata.gpsd).sort();
	var listlength = keylist.length;
//        console.debug([listlength,keylist]);
	for (var count = 0; count < listlength; count++) {
		var key = keylist[count];
//		console.log(key);
		tdop[key] = { x:[], y:[], name: key+" TDoP"};
		nsat[key] = { x:[], y:[], name: key+" nSat", yaxis:"y2"};
		tdop[key].x = jdata.gpsd[key].map(x => date(x[0]));
		nsat[key].x = tdop[key].x;
		tdop[key].y = jdata.gpsd[key].map(x => x[1]);
		nsat[key].y = jdata.gpsd[key].map(x => x[2]);
		statbang(["Summary","local_gps"], nsat[key].y, key + " nSat", "#");
		statbang(["Summary","local_gps"], tdop[key].y, key + " tdop", "*");
		gpsPlot.push(tdop[key]);
		gpsPlot.push(nsat[key]);
	  }
	Plotly.newPlot("ggps", gpsPlot, layoutGPS, {editable: false, scrollZoom: false});
  }

$(document).ready(function(){
    d3_date = d3.utcFormat("%Y-%m-%d %H:%M:%S");
    d3_date2 = d3.timeFormat("%A %B %e %Y, %I:%M:%S %p");
//  console.debug("ready");
  $.getJSON( "js/timey.json", function( data ) {
//    console.debug("fetched");
    jdata=data;
    do_shim();
    host = data.report.host;
    report();
    document.title = host;
    gpsgraph();
    graph_peers();
    graph_all_peer();
    convtemp();
    tempfreqgraph();
    clockfreqgraph();
  });
  window.onresize = function() {
	var plots=["ggps","ljitters","clockfreq","tempfreq","plot_peer_jitters","plot_peers_offsets","graph_time_offset_histogram"];
	for(var i=plots.length-1;i>=0;i--){
		Plotly.Plots.resize($("#"+plots[i])[0]);
	  }
  };
});

async function convtemp(){
	var inner = [];
	var keylist = Object.keys(jdata.temp).sort();
	var listlength = keylist.length;
//        console.debug([listlength,keylist]);
	if (listlength === 0) {
		$("#local_temp_frequency_offset").hide();
		return;
	}
	for (var count = 0; count < listlength; count++) {
		var key = keylist[count];
//		console.log("temp:"+key);
		inner[key] = { x:[], y:[], name: key};
		inner[key].x = jdata.temp[key].map(x => date(x[0]));
		inner[key].y = jdata.temp[key].map(x => x[1]);
		statbang(["Summary","local_temp_frequency_offset"], inner[key].y, key, "°C",);
		tempfreqPlot.push(inner[key]);
	  }
  }
function clipdig(num, digits) {
    return Math.trunc(num*(10**digits))/(10**digits);
}
async function statbang(parents, statin, name, unit) {
	var i, k = statin;
	k.filter(k => "number" === typeof(k));
	var ml = ["tr", ["td", name]]
	var out = generate_stats(k);
	for (i = 0; i < 13; i++) {
		ml.push(['td', clipdig(out[i], 4)]);
	  }
	ml.splice(13,0,['td', unit]);
	ml.splice(12,0,['td', '']);
	ml.splice(9,0,['td', '']);
	for (i in parents) {
		var keyed = d3.select("#" + parents[i] + " tbody");
		jqml([keyed._groups[0][0], [ml]]);
	  }
}

function clockfreqgraph(){
	loffsetPlot = [ { x:[], y:[], name: "clock offset s"},
		      { x:[], y:[], name: "frequency offset ppm", yaxis: "y2"}];
	ljitterPlot = [ { x:[], y:[], name: "clock jitter ms"},
		       { x:[], y:[], name: "frequency jitter ppm", yaxis: "y2"}];
	histoPlot =  [ { x:[], name: "clock offset s", type: "histogram"}];
//	console.log("loop");
	loffsetPlot[0].x = jdata.loop.map(x => date(x[0]));
	loffsetPlot[1].x = loffsetPlot[0].x;
	ljitterPlot[1].x = loffsetPlot[0].x;
	ljitterPlot[1].x = loffsetPlot[0].x;
	loffsetPlot[0].y = jdata.loop.map(x => x[1]);
	loffsetPlot[1].y = jdata.loop.map(x => x[2]);
	ljitterPlot[0].y = jdata.loop.map(x => x[3]);
	ljitterPlot[1].y = jdata.loop.map(x => x[4]);
	histoPlot[0].x = loffsetPlot[0].y;
	statbang(["Summary","local_time_frequency_offset","local_clock_time_offset_histogram"], loffsetPlot[0].y, "clock offset", "s");
	statbang(["Summary","local_time_frequency_offset","local_temp_frequency_offset"], loffsetPlot[1].y, "frquency offset", "ppm");
	statbang(["Summary","local_time_frequency_jitter"], ljitterPlot[0].y, "clock jitter", "ms");
	statbang(["Summary","local_time_frequency_jitter"], ljitterPlot[1].y, "frequency jitter", "ppm");
	Plotly.newPlot("ljitters", ljitterPlot, layoutLJitters, {editable: false, scrollZoom: false});
	Plotly.newPlot("clockfreq", loffsetPlot, layoutClockFreq, {editable: false, scrollZoom: false});
	Plotly.newPlot("graph_time_offset_histogram", histoPlot, layout_histo, {editable: false, scrollZoom: false});
  }
function tempfreqgraph(){
	if (Object.keys(tempfreqPlot).length === 0) {
		$("#local_temp_frequency_offset").hide();
		return;
	}
	var inner = { x:[], y:[], name: "frequency offset ppm", yaxis: "y2"};
	inner.x = jdata.loop.map(x => date(x[0]));
	inner.y = jdata.loop.map(x => x[2]);
	tempfreqPlot.push(inner);
	return Plotly.newPlot("tempfreq", tempfreqPlot, layoutTempFreq, {editable: false, scrollZoom: false});
  }

async function graph_peers(){
	var base, prep = ["peer offset ", "peer jitter ", "refclock offset ", "refclock jitter "];
	var offset = [];
	var jitter = [];
	var keylist = Object.keys(jdata.peer).sort();
	var listlength = keylist.length;
//	console.debug([listlength,keylist]);
	if (listlength === 0) {
		$("#peer_jitters").hide();
		$("#peer_offsets").hide();
		return;
	}
	for (var count = 0; count < listlength; count++) {
		var key = keylist[count];
//		console.log("peer:"+key);
		jitter[key] = { x:[], y:[], name: key};
		offset[key] = { x:[], y:[], name: key};
		jitter[key].x = jdata.peer[key].map(x => date(x[0]));
		offset[key].x = jitter[key].x;
		jitter[key].y = jdata.peer[key].map(x => x[4]);
		offset[key].y = jdata.peer[key].map(x => x[1]);
		if(0 > key.indexOf(")") && 0 != key.indexOf("127.127.")) {
			base = 0;
		  } else {
			base = 2;
		  }
		statbang(["Summary","peer_jitters"], jitter[key].y, prep[base + 1] + key, "ppm");
		statbang(["Summary","peer_offsets"], offset[key].y, prep[base] + key, "ms");
		peersjitterPlot.push(jitter[key]);
		peersoffsetPlot.push(offset[key]);
	  }
	Plotly.newPlot("plot_peer_jitters", peersjitterPlot, layoutJitters, {editable: false, scrollZoom: false});
	Plotly.newPlot("plot_peers_offsets", peersoffsetPlot, layoutOffsets, {editable: false, scrollZoom: false});
  }

async function graph_all_peer(){
	var keylist = Object.keys(jdata.peer).sort();
	var listlength = keylist.length;
	for (var count = 0; count < listlength; count++) {
		var key = keylist[count];
		
      }
}

async function report(){
	$("#host")[0].value=jdata.report.host;
	$("#end")[0].value=date2(jdata.report.end);
	$("#start")[0].value=date2(jdata.report.start);
	$("#generated")[0].value=date2(jdata.report.generated);
	$("#period")[0].value=clipdig(jdata.report.period/86400,3) + " Days ";
}

// sort numbers in ascending order
function ascNum(a, b) {
	return a - b;
  }

// calculate the nth percentile
function percentile(_arr, k) {
  var realIndex = k * (_arr.length - 1);
  var index = parseInt(realIndex);
  var frac = realIndex - index;

  if (index + 1 < _arr.length) {
    return _arr[index] * (1 - frac) + _arr[index + 1] * frac;
  } else {
    return _arr[index];
  }
}

function generate_stats(argument) {
	var min, p1, p5, p50, p95, p99, max, r90, r98;
	var mean, variance = 0, stddev, kurt = 0, skew = 0, loop;
    var dist = argument.slice().sort(ascNum);

	// number of elements in the array
	var lenny = dist.length;

	// first and last should be min and max
	min  = dist[0]
	max  = dist[lenny-1]

	// calculate percentiles
	p1   = percentile(dist, 0.01)
	p5   = percentile(dist, 0.05)
	p50  = percentile(dist, 0.5)
	p95  = percentile(dist, 0.95)
	p99  = percentile(dist, 0.99)

	// calculate ranges
	r90  = p95 - p5;
	r98  = p99 - p1;

	mean = dist.reduce((a,b) => a+b) / lenny;

	// some things say length others length - 1
	variance = dist.map(x => Math.pow((x - mean),2)).reduce((a,b) => a+b) / lenny;
	stddev = Math.sqrt(variance);

	skew = dist.map(x => Math.pow((x - mean)/stddev,3)).reduce((a,b) => a+b) / lenny;
	kurt = dist.map(x => Math.pow((x - mean)/stddev,4)).reduce((a,b) => a+b) / lenny - 3;

	return [min, p1, p5, p50, p95, p99, max, r90, r98, stddev, mean, skew, kurt];
  }

  function do_shim() {
    var shimmy = { "xaxis": {"showgrid": true, "gridcolor": "rgba(0,0,0,0.5)"}}
	layoutClockFreq = Object.assign(layoutClockFreq, shimmy);
	layoutTempFreq = Object.assign(layoutTempFreq, shimmy);
	layoutGPS  = Object.assign(layoutGPS , shimmy);
	layoutJitters  = Object.assign(layoutJitters , shimmy);
	layoutOffsets  = Object.assign(layoutOffsets , shimmy);
	layoutLJitters = Object.assign(layoutLJitters, shimmy);
//	layout_histo = Object.assign(layout_histo, shimmy);
}
