self.addEventListener('install', function(e) {
  e.waitUntil(
    caches.open('timey2-cache-0').then(function(cache) {
      return cache.addAll([
        '/public-html/',
        '/public-html/timey2.html',
        '/public-html/imgs/favicon.ico',
        '/public-html/imgs/ntpsec-logo.png',
        '/public-html/js/manifest.json',
        '/public-html/js/timey2.js',
        '/public-html/js/timey2-service.js',
        '/public-html/js/jqmlns.js',
//        '',
//        '',
//        '',
//        '',
        '/public-html/js/timey.json'
      ]);
    })
  );
});

