#!/usr/bin/python3.6
import json
import gps.client

if __name__ == '__main__':

    clip_digits = 3

    tpv_clip = ['magtrack', 'track', 'lat', 'lon', 'alt', 'epx', 'epy',
                'epv', 'eps', 'epd', 'epc', 'ept', 'speed', 'climb']

    sky_clip = ['xdop', 'ydop', 'vdop', 'cdop', 'sdop', 'edop', 'tdop']

    print('''\
Content-Type: text/event-stream
Access-Control-Allow-Origin: *
Cache-Control: no-cache
''')

    with open('/var/www/localhost/htdocs/james/gpsd-c.json',
              'r', encoding='utf-8') as fp:
        points = json.load(fp)
    inny = {"class": "inital_tracking", "points": points}
    print('data: %s\n' % json.dumps(inny), flush=True)

    session = gps.gps(mode=gps.WATCH_ENABLE|gps.WATCH_JSON|gps.WATCH_PPS,
                      host='127.0.0.1', port=2947)
    while True:
        report = session.next()
        inny = json.loads(session.response)
        if inny.get('class', '') in ['TPV', 'SKY']:
            if inny['class'] == 'TPV':
                 for key in tpv_clip:
                    if key in inny:
                        inny[key] = round(inny[key], clip_digits)
            elif inny['class'] == 'SKY':
                for char in sky_clip:
                    if key in inny:
                        inny[key] = round(inny[key], clip_digits)
            inny['class'] = inny['class'].lower()
        print('data: %s\n' % json.dumps(inny), flush=True)
