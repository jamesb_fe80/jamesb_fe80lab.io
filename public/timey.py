#!/usr/bin/python3
# -*- coding: utf-8 -*-
# SPDX-License-Identifier: BSD-2-Clause
"""Generate a JSON report of NTP server statistics bounded by (optional) query."""
from zlib import compress
from json import dumps
from socket import getfqdn
from time import time
import wsgiref.handlers
from ntp.statfiles import NTPStats

if str is bytes:
    from urlparse import parse_qs
else:
    from urllib.parse import parse_qs

KILLTIME = 2200
DEFAULT_DAYS = 1
SECONDS_PER_DAY = 86400
STATS_DIR = '/var/log/ntpstats/'


def etl_gpsd(record):
    """Extract and Load GPSd data."""
    return (int(record[1]), float(record[3]), int(record[4]))


def etl_temp(record):
    """Extract and Load Server Temperatures."""
    return (int(record[1]), float(record[3]))


def etl_peer(record):
    """Extract and Load Peer Statistics."""
    return (int(float(record[1])), float(record[4]), float(record[5]),
            float(record[6]), float(record[7]))


def etl_loop(record):
    """Extract and Load Local Loop statistics."""
    return (int(float(record[1])), float(record[2]), float(record[3]),
            float(record[4]), float(record[5]), float(record[6]))


def etl_pass(record):
    """Null operation: return the sole argument for passthrough."""
    return record


def do_records(table, points, etl):
    """Given the input timed 2d array process values through 'etl' to produce the output timed 2d array."""
    stale = 0
    entries = []
    for record in table:
        fields = etl(record)
        if (stale != 0) and (stale < (fields[0] - KILLTIME)):
            new = [stale+(KILLTIME/2)]
#           If silent too long inject a null value to break up the graph
            for count in range(points):
                new += [None]
                count *= 1
            entries += [new]
        entries += [fields]
        stale = fields[0]
    return entries


def generate_report(records):
    """Given a complex object feed it into pocessing and get a cooked JSON string."""
    gpsd = records.gpssplit()
    temp = records.tempssplit()
    peer = records.peersplit()
    loop = records.loopstats

    the_list = list(gpsd.keys())
    the_list.sort()
    gpso = {}
    for key in the_list:
        gpso[key] = do_records(gpsd[key], 2, etl_gpsd)

    the_list = list(temp.keys())
    the_list.sort()
    tempo = {}
    for key in the_list:
        tempo[key] = do_records(temp[key], 1, etl_temp)

    the_list = list(peer.keys())
    the_list.sort()
    peero = {}
    for key in the_list:
        peero[key] = do_records(peer[key], 4, etl_peer)

    loopo = do_records(loop, 5, etl_loop)

    return {"gpsd": gpso, "temp": tempo, "loop": loopo, "peer": peero}


def parse_queries(queries):
    """Given a set of form/cgi queries generate a valid start and end time with a period in between."""
    tai = int(time())
    (end, period, start) = (tai, SECONDS_PER_DAY * DEFAULT_DAYS, 0)
    given = []
    try:
        for key in queries:
            if key == 'end':
                given += ["end"]
                end = int(float(queries[key][0]))
            if key == 'period':
                given += ["period"]
                period = int(float(queries[key][0]))
                if period < 30:
                    period *= SECONDS_PER_DAY
            if key == 'start':
                given += ["start"]
                start = int(float(queries[key][0]))
    except ValueError:
        pass

    if end > tai:  # No future value projections
        end = tai
    if start > tai:  # No future value projections
        start = tai

    if end < start:  # never start after we end, nor end before we begin
        (end, start) = (start, end)
    if start != 0 and end != 0:  # If we don't know the duration is from beginning to end
        period = end - start
    elif start == 0:  # If we don't know when we started it is the the duration before the end
        start = end - period

    return (end, period, start, given)


def application(environ, start_response):
    """Make sausages."""
    if environ["REQUEST_METHOD"] == "GET":
        queries = parse_qs(environ['QUERY_STRING'])
    elif environ["REQUEST_METHOD"] == "POST":
        try:
            request_body_size = int(environ.get('CONTENT_LENGTH', 0))
        except ValueError:
            request_body_size = 0
        request_body = environ['wsgi.input'].read(request_body_size)
        queries = parse_qs(request_body)
    else:
        response_body = "The Requested methdod is not supported at this time."
        response_body = response_body.encode('utf-8')
        start_response('400 Bad Request', [('Content-Type', 'text/plain'),
                                           ('Content-Length', str(len(response_body)))])
        return [response_body]

    (end, period, start, given) = parse_queries(queries)

    query_results = NTPStats(STATS_DIR, starttime=start, endtime=end)

# query_results = NTPStats(STATS_DIR,
# endtime=1533452337, starttime=1533366213)
# period=int(SECONDS_PER_DAY * DEFAULT_DAYS))
# period=int(1800))

    out = generate_report(query_results)
    out["report"] = {'host': getfqdn(), 'end': end, 'period': period,
                     'start': start, 'generated': int(time()), "given": given}

#    response_body = dumps(out, sort_keys=True, indent=2)
#    response_body = dumps(out)
#    response_body = bytes(dumps(out, sort_keys=True, indent=0).encode('utf-8'))
    s_in = dumps(out).encode('utf-8')
    response_body = s_in
#    response_body = compress(s_in)

    status = '200 OK'
    response_headers = [
        ('Access-Control-Allow-Origin', '*'),
        ('Content-Type', 'text/json'),
        ('Content-Length', str(len(response_body))),
#        ('Content-Encoding', 'gzip')
    ]
    start_response(status, response_headers)
    return [response_body]


def blunk(status, response_headers):
    """Print the status and the response header in a callback function."""
    print('status:\t%s' % status)
    for header in response_headers:
        print('%s:\t%s' % (header[0], header[1]))
    print('')


if __name__ == '__main__':
    wsgiref.handlers.CGIHandler().run(application)
