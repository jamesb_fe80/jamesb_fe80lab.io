#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""Return z compressed resource via the web."""
from gzip import open as zopen
from os.path import exists, getmtime, getctime, isfile
## from sys import stderr
from shutil import copyfileobj
from time import gmtime, mktime, strftime, strptime
## from urllib.parse import parse_qs
import wsgiref.handlers


def application(environ, start_response):
    """Do all of the work."""
    if 'PATH_INFO' not in environ:
        response_body = "No PATH_INFO in the request. What sent that..."
        response_body = response_body.encode('utf-8')
        start_response('400 Bad Request', [('Content-Type', 'text/plain'),
                                           ('Content-Length',
                                            str(len(response_body)))])
        return [response_body]

    path = environ['PATH_INFO']
    parts = path.split("/")
    if '..' in parts:
        response_body = "Naughty hackers, trying to ascend the tree..."
        response_body = response_body.encode('utf-8')
        start_response('400 Bad Request', [('Content-Type', 'text/plain'),
                                           ('Content-Length',
                                            str(len(response_body)))])
        return [response_body]
    zpath = '/var/www/localhost/htdocs' + path + ".gz"
    fpath = '/var/www/localhost/htdocs' + path
    if exists(fpath) and isfile(fpath):
        if exists(zpath) and isfile(zpath):
            if getmtime(zpath) < getmtime(fpath):
                with open(fpath, 'rb') as f_in:
                    with zopen(zpath, 'wb') as z_out:
                        copyfileobj(f_in, z_out)

    if exists(fpath) and isfile(fpath) and not exists(zpath):
        with open(fpath, 'rb') as f_in:
            with zopen(zpath, 'wb') as z_out:
                copyfileobj(f_in, z_out)
    if exists(zpath) and isfile(zpath):
        if 'HTTP_IF_MODIFIED_SINCE' in environ:
            rtime = strptime(environ['HTTP_IF_MODIFIED_SINCE'], "%a, %d %b %Y %X %Z")
            if mktime(rtime) < getctime(zpath):
                response_body = b""
                status = '304 Not Modified'
                response_headers = [
                    # ("ETag", "-b"),
                    ("RTAI", "%d" % mktime(rtime)),
                    ("FTAI", "%d" % getctime(zpath)),
                    ("Date", strftime("%a, %d %b %Y %X %Z",
                                      gmtime(getctime(zpath))))
                    ]
                start_response(status, response_headers)
                return [response_body]
        if environ["REQUEST_METHOD"] in ['GET', 'POST']:
            with open(zpath, "rb") as z_in:
                response_body = z_in.read()
                status = '200 OK'
                response_headers = [
                    ('Access-Control-Allow-Origin', '*'),
                    ('Content-Type', 'text/json'),
                    ('Content-Length', str(len(response_body))),
                    # "%a, %d %b %Y %H:%M:%S %Z"
                    ("Last-Modified", strftime("%a, %d %b %Y %X %Z",
                                               gmtime(getctime(zpath)))),
                    # ("ETag", "-b"),
                    ('Content-Encoding', 'gzip')
                    ]
                start_response(status, response_headers)
                return [response_body]

    response_body = ("The requested file \"%s\" could not be found." %
                     path).encode('utf-8')

    status = '200 OK'
    response_headers = [
        ('Access-Control-Allow-Origin', '*'),
        ('Content-Type', 'text/json'),
        ('Content-Length', str(len(response_body)))
    ]
    start_response(status, response_headers)
    return [response_body]


def blunk(status, response_headers):
    """Print the status and the response header in a callback function."""
    print('status:\t%s' % status)
    for header in response_headers:
        print('%s:\t%s' % (header[0], header[1]))
    print('')


if __name__ == '__main__':
    wsgiref.handlers.CGIHandler().run(application)
