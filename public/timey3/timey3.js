"use strict";
let src_url = "https://dell-2018.jamesb192.com/cgi-bin/timey.py"
let host = "_";
let tFormat = "Y-MM-DD HH:mm:ss";
let d3_date;
let d3_date2;

let jdata = {};

let gpsPlot = [];
let tempfreqPlot = [];
let loffsetPlot = [];
let peersoffsetPlot = [];
let peersjitterPlot = [];
let ljitterPlot = [];
let histoPlot = [];
let stabbyPlot = [];

let daystore = {};
let initted = {};

let layoutClockFreq = {
	title : host + ": Local Clock/Frequency Offset",
	showLegend : false,
	yaxis : {title : "s", autorange : false, fixedrange : true},
	yaxis2 : {
		title : "ppm",
		overlaying : "y",
		side : "right",
		autorange : false,
		fixedrange : true
	}
};
let layoutTempFreq = {
	title : host + ": Local Frequency Offset/Temps",
	showLegend : false,
	yaxis : {
		"showgrid" : true,
		"gridcolor" : "rgba(0,0,0,0.5)",
		title : "&deg;C",
		fixedrange : true
	},
	yaxis2 : {
		"showgrid" : true,
		"gridcolor" : "rgba(0,0,0,0.5)",
		title : "ppm",
		overlaying : "y",
		side : "right",
		fixedrange : true
	}
};
let layoutGPS = {
	title : host + ": Local GPS",
	yaxis : {
		"showgrid" : true,
		overlaying : "y2",
		"gridcolor" : "rgba(0,0,0,0.5)",
		title : "TDOP",
		fixedrange : true
	},
	yaxis2 : {
		"showgrid" : true,
		"gridcolor" : "rgba(0,0,0,0.5)",
		title : "nSat",
		side : "right",
		fixedrange : true
	}
};
let layoutJitters = {
	title : host + ": Peer Jitters",
	yaxis : {
		"showgrid" : true,
		"gridcolor" : "rgba(0,0,0,0.5)",
		title : "ms",
		fixedrange : true
	},
	overlaying : "y",
	side : "left",
	fixedrange : true
};
let layoutOffsets = {
	title : host + ": Peer Offsets",
	yaxis : {
		"showgrid" : true,
		"gridcolor" : "rgba(0,0,0,0.5)",
		title : "ms",
		fixedrange : true
	},
	overlaying : "y",
	side : "left",
	fixedrange : true
};
let layoutLJitters = {
	title : host + ": Local Clock Time/Frequency Jitters",
	xaxis : {
		"showgrid" : true,
		"gridcolor" : "rgba(0,0,0,0.5)",
		title : "ms",
		fixedrange : true
	},
	yaxis2 : {
		overlaying : "y",
		title : "ppm",
		fixedrange : true,
		side : "right"
	},
	fixedrange : true
};
let layout_histo = {title : host + ": Local Clock Time Offset Histogram"};
let layout_stabby = {
	title : "Stabbiness",
	yaxis :
	    {showgrid : true, gridcolor : "rgba(0,0,0,0.5)", fixedrange : true}
};

let selectorOptions = {
	buttons : [
		{step : "day", stepmode : "backward", count : 1, label : "1d"},
		{step : "week", stepmode : "backward", count : 1, label : "1w"},
		{
			step : "all",
		}
	],
};

let deferredPrompt;

if ('serviceWorker' in navigator) {
  navigator.serviceWorker
    .register('sw.js')
    .then(() => { console.log('Service Worker Registered'); });
}

function date(tai) { return d3_date(tai * 1000); }
function date2(tai) { return d3_date2(tai * 1000); }

function genstab(x, y) {
    if ("numbernumber" == typeof (x) + typeof (y)) {
	var n1 = Math.sqrt(((x*x)+(y*y))/2);
	var n2 = Math.sqrt(Math.abs((x*x)-(y*y))/2);
	return 1 - (n2 / n1);
    } else {
	return null;
    }
}

function graph_stabby() {
	var keylist = Object.keys(jdata.peer).sort();
	var listlength = keylist.length;
	for (var count = 0; count < listlength; count++) {
		var blob = jdata.peer[keylist[count]];
		var outy = {x : [], y : [], name : keylist[count]+' ot'};
		var outz = {x : [], y : [], name : keylist[count]+' of'};
		for (var pointer = blob.length - 2; pointer > 0; pointer--) {
			outy.y[pointer] = genstab(
			    blob[pointer][1], blob[pointer + 1][1]);
			outz.y[pointer] = genstab(
			    blob[pointer][4], blob[pointer + 1][4]);
			outy.x[pointer] =
			    date((blob[pointer][0] + blob[pointer + 1][0]) / 2);
		}
		outz.x = outy.x;
		statbang([ "stabby_d" ], outy.y, keylist[count]+' ot', "?");
		statbang([ "stabby_d" ], outz.y, keylist[count]+' of', "?");
		stabbyPlot.push(outy);
		stabbyPlot.push(outz);
	}
	var outv = {x : [], y : [], name : 'local ot'};
	var outw = {x : [], y : [], name : 'local of'};
	var outy = {x : [], y : [], name : 'local jt'};
	var outz = {x : [], y : [], name : 'local jf'};
	var blob = jdata.loop;
	for (var pointer = blob.length - 2; pointer > 0; pointer--) {
		outv.y[pointer] =
		    genstab(blob[pointer][1], blob[pointer + 1][1]);
		outw.y[pointer] =
		    genstab(blob[pointer][2], blob[pointer + 1][2]);
		outy.y[pointer] =
		    genstab(blob[pointer][3], blob[pointer + 1][3]);
		outz.y[pointer] =
		    genstab(blob[pointer][4], blob[pointer + 1][4]);
		outv.x[pointer] =
		    date((blob[pointer][0] + blob[pointer + 1][0]) / 2);
	}
	outz.x = outy.x = outw.x = outv.x;
	stabbyPlot.push(outv);
	stabbyPlot.push(outw);
	stabbyPlot.push(outy);
	stabbyPlot.push(outz);
	statbang([ "stabby_d" ], outv.y, 'local clock ot', "?");
	statbang([ "stabby_d" ], outw.y, 'local clock of', "?");
	statbang([ "stabby_d" ], outy.y, 'local clock jt', "?");
	statbang([ "stabby_d" ], outz.y, 'local clock jf', "?");
	Plotly.newPlot("plot_stabbieness", stabbyPlot, layout_stabby,
		       {editable : false, scrollZoom : false})
}

async function gpsgraph() {
	var tdop = [];
	var nsat = [];
	var keylist = Object.keys(jdata.gpsd).sort();
	var listlength = keylist.length;
	for (var count = 0; count < listlength; count++) {
		var key = keylist[count];
		/* console.log(key); */
		tdop[key] = {x : [], y : [], name : key + " TDoP"};
		nsat[key] = {
			x : [],
			y : [],
			name : key + " nSat",
			yaxis : "y2",
			type : "bar",
			marker : {opacity : 0.5}
		};
		tdop[key].x = jdata.gpsd[key].map(x => date(x[0]));
		nsat[key].x = tdop[key].x;
		tdop[key].y = jdata.gpsd[key].map(x => x[1]);
		nsat[key].y = jdata.gpsd[key].map(x => x[2]);
		statbang([ "Summary", "local_gps" ], nsat[key].y, key + " nSat",
			 "#");
		statbang([ "Summary", "local_gps" ], tdop[key].y, key + " tdop",
			 "*");
		gpsPlot.push(nsat[key]);
		gpsPlot.push(tdop[key]);
	}
	Plotly.newPlot("ggps", gpsPlot, layoutGPS,
		       {editable : false, scrollZoom : false});
}

function fetchday(mud, urb) {
	var strung = urb + "?start=" + (mud * 86400).toString() +
		     "&end=" + (86399 + mud * 86400);
	var categories = [ "temp", "peer", "gpsd" ];
	$.getJSON( strung, function( data ) {
		daystore[mud.toString()] = data;
		var days = Object.keys(daystore).sort();
		jdata = {};
		if (host == "_") {
			host = data.report.host;
			layoutClockFreq['title'] =
			    host + ": Local Clock/Frequency Offset";
			layoutTempFreq['title'] =
			    host + ": Local Frequency Offset/Temps";
			layoutGPS['title'] = host + ": Local GPS";
			layoutJitters['title'] = host + ": Peer Jitters";
			layoutOffsets['title'] = host + ": Peer Offsets";
			layoutLJitters['title'] =
			    host + ": Local Clock Time/Frequency Jitters";
			layout_histo['title'] =
			    host + ": Local Clock Time Offset Histogram";
		}
		for(var c1 in categories){
		var category = categories[c1];
		jdata[category] = {};
		for (var c2 in days) {
			var day = days[c2];
			for (var c3 in daystore[day][category]) {
				if ("undefined" ==
				    typeof (jdata[category][c3])) {
					jdata[category][c3] = [];
				}
				jdata[category][c3] =
				    jdata[category][c3]
					.concat(daystore[day][category][c3])
					.sort();
				}
			}
		}
		jdata.loop=[];
		jdata.report = {'end':0,'period':0,'start':Date.now()};
		for(var c1 in days){
		var day = days[c1];
		jdata.loop = jdata.loop.concat(daystore[day].loop);
		jdata.report.start =
		    Math.min(daystore[day].report.start, jdata.report.start);
		jdata.report.end =
		    Math.max(daystore[day].report.end, jdata.report.end);
		jdata.report.period =
		    daystore[day].report.period + jdata.report.period + 1;
		jdata.report.host = daystore[day].report.host;
		}
		jdata.report.generated = Math.floor(Date.now()/1000);
		gpsPlot = [];
		tempfreqPlot = [];
		loffsetPlot =[];
		peersoffsetPlot = [];
		peersjitterPlot = [];
		ljitterPlot = [];
		histoPlot = [];
		stabbyPlot = [];
		$("tbody").empty();
		report();
		document.title = host;
		if ($("#Sensors_i")[0].checked) {
			gpsgraph();
			convtemp();
			tempfreqgraph();
		}
		if ($("#Peers_i")[0].checked) {
			graph_peers();
			graph_all_peer();
		}
		if ($("#Local_i")[0].checked) {
			clockfreqgraph();
		}
		if ($("#stabby_i")[0].checked) {
			graph_stabby();
		}
	});
}



$(document).ready(function() {
	d3_date = d3.utcFormat("%Y-%m-%d %H:%M:%S");
	d3_date2 = d3.timeFormat("%A %B %e %Y, %I:%M:%S %p");
	do_shim();
	var mud = Math.floor(Date.now() / 86400000);
	fetchday(mud, src_url);
	fetchday(mud - 1, src_url);
	mud -= 2;
	window.onresize = function() {
		var plots = [
			"plot_stabbieness", "ggps", "ljitters", "clockfreq",
			"tempfreq", "plot_peer_jitters", "plot_peers_offsets",
			"graph_time_offset_histogram"
		];
		for (var i = plots.length - 1; i >= 0; i--) {
			Plotly.Plots.resize($("#" + plots[i])[0]);
		}
	};
	if (localStorage.getItem('theme') === 'dark') {
                document.getElementById("slider").checked = false;
		setTheme('dark');
	} else {
                document.getElementById("slider").checked = true;
		setTheme('light');
	}
	$("#fetch").click(function(e) {
		fetchday(mud, src_url);
		mud--;
	});
	$("#Sensors_i").click(function(e) {
		if(!$("#Sensors_i")[0].checked) { return; }
		if ('Sensors_i' in initted) { return; }
		initted['Sensors_i']=true;
		console.log("Sensors_i");
		gpsgraph();
		convtemp();
		tempfreqgraph();
	});
	$("#Peers_i").click(function(e) {
		if(!$("#Peers_i")[0].checked) { return; }
		if ('Peers_i' in initted) { return; }
		initted['Peers_i']=true;
		console.log("Peers_i");
		graph_peers();
		graph_all_peer();
	});
	$("#Local_i").click(function(e) {
		if(!$("#Local_i")[0].checked) { return; }
		if ('Local_i' in initted) { return; }
		initted['Local_i']=true;
		console.log("Local_i");
		clockfreqgraph();
	});
	$("#stabby_i").click(function(e) {
		if(!$("#stabby_i")[0].checked) { return; }
		if ('stabby_i' in initted) { return; }
		initted['stabby_i']=true;
		console.log("stabby_i");
		graph_stabby();
	});
});

window.addEventListener('beforeinstallprompt', (e) => {
	e.preventDefault();
	deferredPrompt = e;
	var addBtn = $("#install")[0];
	addBtn.style.display = 'block';

	addBtn.addEventListener('click', () => {
		addBtn.style.display = 'none';
		deferredPrompt.prompt();
		deferredPrompt.userChoice.then((choiceResult) => {
			deferredPrompt = null;
		});
	});
});

// function to set a given theme/color-scheme
function setTheme(themeName) {
	localStorage.setItem('theme', themeName);
	document.body.className = themeName;
}

// function to toggle between light and dark theme
function toggleTheme() {
	if (localStorage.getItem('theme') === 'dark') {
		    setTheme('light');
	} else {
		    setTheme('dark');
	}
}

async function convtemp() {
	var inner = [];
	var keylist = Object.keys(jdata.temp).sort();
	var listlength = keylist.length;
	if (listlength === 0) {
		$("#local_temp_frequency_offset").hide();
		return;
	}
	for (var count = 0; count < listlength; count++) {
		var key = keylist[count];
		/* console.log("temp:"+key); */
		inner[key] = {x : [], y : [], name : key};
		inner[key].x = jdata.temp[key].map(x => date(x[0]));
		inner[key].y = jdata.temp[key].map(x => x[1]);
		statbang(
		    [ "Summary", "local_temp_frequency_offset" ],
		    inner[key].y,
		    key,
		    "°C",
		);
		tempfreqPlot.push(inner[key]);
	}
}
function clipdig(num, digits) {
	return Math.trunc(num * (10 ** digits)) / (10 ** digits);
}
async function statbang(parents, statin, name, unit) {
	var i, k = statin;
	k.filter(k => "number" === typeof (k));
	var ml = [ "tr", [ "td", name ] ];
	var out = generate_stats(k);
	for (i = 0; i < 13; i++) {
		ml.push([ 'td', clipdig(out[i], 4) ]);
	}
	ml.splice(13, 0, [ 'td', unit ]);
	ml.splice(12, 0, [ 'td', '' ]);
	ml.splice(9, 0, [ 'td', '' ]);
	for (i in parents) {
		var keyed = d3.select("#" + parents[i] + " tbody");
		jqml([ keyed._groups[0][0], [ ml ] ]);
	}
}

function clockfreqgraph() {
	loffsetPlot = [
		{x : [], y : [], width : [], name : "clock offset s"}, {
			x : [],
			y : [],
			width : [],
			name : "frequency offset ppm",
			yaxis : "y2"
		}
	];
	ljitterPlot = [
		{x : [], y : [], name : "clock jitter ms"},
		{x : [], y : [], name : "frequency jitter ppm", yaxis : "y2"}
	];
	histoPlot = [ {x : [], name : "clock offset s", type : "histogram"} ];
	/* console.log("loop"); */
	loffsetPlot[0].x = jdata.loop.map(x => date(x[0]));
	loffsetPlot[1].x = loffsetPlot[0].x;
	ljitterPlot[0].x = loffsetPlot[0].x;
	ljitterPlot[1].x = loffsetPlot[0].x;
	loffsetPlot[0].y = jdata.loop.map(x => x[1]);
	loffsetPlot[1].y = jdata.loop.map(x => x[2]);
	ljitterPlot[0].y = jdata.loop.map(x => x[3]);
	ljitterPlot[1].y = jdata.loop.map(x => x[4]);
	histoPlot[0].x = loffsetPlot[0].y;
	var extreme;
	//extreme = Math.max.apply(null, jdata.loop.map(x => Math.abs(x[1])));
	layoutClockFreq.yaxis.range = [
	    Math.min.apply(null, loffsetPlot[0].y),
	    Math.max.apply(null, loffsetPlot[0].y)
	];
	//extreme = Math.max.apply(null, jdata.loop.map(x => Math.abs(x[2])));
	layoutClockFreq.yaxis2.range = [
	    Math.min.apply(null, loffsetPlot[1].y),
	    Math.max.apply(null, loffsetPlot[1].y)
	];
	statbang(
	    [
		    "Summary", "local_time_frequency_offset",
		    "local_clock_time_offset_histogram"
	    ],
	    jdata.loop.map(x => x[1] * 1e6), "clock offset", "us");
	statbang(
	    [
		    "Summary", "local_time_frequency_offset",
		    "local_temp_frequency_offset"
	    ],
	    jdata.loop.map(x => x[2]), "frquency offset", "ppm");
	statbang([ "Summary", "local_time_frequency_jitter" ], ljitterPlot[0].y,
		 "clock jitter", "ms");
	statbang([ "Summary", "local_time_frequency_jitter" ], ljitterPlot[1].y,
		 "frequency jitter", "ppm");
	Plotly.newPlot("ljitters", ljitterPlot, layoutLJitters,
		       {editable : false, scrollZoom : false});
	Plotly.newPlot("clockfreq", loffsetPlot, layoutClockFreq,
		       {editable : false, scrollZoom : false});
	Plotly.newPlot("graph_time_offset_histogram", histoPlot, layout_histo,
		       {editable : false, scrollZoom : false});
}

function tempfreqgraph() {
	if (Object.keys(tempfreqPlot).length === 0) {
		$("#local_temp_frequency_offset").hide();
		return;
	}
	var inner =
	    {x : [], y : [], name : "frequency offset ppm", yaxis : "y2"};
	inner.x = jdata.loop.map(x => date(x[0]));
	inner.y = jdata.loop.map(x => x[2]);
	tempfreqPlot.push(inner);
	return Plotly.newPlot("tempfreq", tempfreqPlot, layoutTempFreq,
			      {editable : false, scrollZoom : false});
}

async function graph_peers() {
	var base, prep = [
		"peer offset ", "peer jitter ", "refclock offset ",
		"refclock jitter "
	];
	var offset = [];
	var jitter = [];
	var keylist = Object.keys(jdata.peer).sort();
	var listlength = keylist.length;
	if (listlength === 0) {
		$("#peer_jitters").hide();
		$("#peer_offsets").hide();
		return;
	}
	for (var count = 0; count < listlength; count++) {
		var key = keylist[count];
		/* console.log("peer:"+key); */
		jitter[key] = {x : [], y : [], name : key};
		offset[key] = {x : [], y : [], name : key};
		jitter[key].x = jdata.peer[key].map(x => date(x[0]));
		offset[key].x = jitter[key].x;
		jitter[key].y = jdata.peer[key].map(x => x[4]);
		offset[key].y = jdata.peer[key].map(x => x[1]);
		if (0 > key.indexOf(")") && 0 != key.indexOf("127.127.")) {
			base = 0;
		} else {
			base = 2;
		}
		statbang([ "Summary", "peer_jitters" ], jdata.peer[key].map(x => x[4] * 1e3),
			 prep[base + 1] + key, "us");
		statbang([ "Summary", "peer_offsets" ], jdata.peer[key].map(x => x[1] * 1e3),
			 prep[base] + key, "us");
		peersjitterPlot.push(jitter[key]);
		peersoffsetPlot.push(offset[key]);
	}
	Plotly.newPlot("plot_peer_jitters", peersjitterPlot, layoutJitters,
		       {editable : false, scrollZoom : false});
	Plotly.newPlot("plot_peers_offsets", peersoffsetPlot, layoutOffsets,
		       {editable : false, scrollZoom : false});
}

async function graph_all_peer() {
	var keylist = Object.keys(jdata.peer).sort();
	var listlength = keylist.length;
	for (var count = 0; count < listlength; count++) {
		var key = keylist[count];
	}
}

async function report() {
	$("#host")[0].value = jdata.report.host;
	$("#end")[0].value = date2(jdata.report.end);
	$("#start")[0].value = date2(jdata.report.start);
	$("#generated")[0].value = date2(jdata.report.generated);
	$("#period")[0].value = clipdig(jdata.report.period / 86400, 3) + " Days ";
}

/* sort numbers in ascending order */
function ascNum(a, b) { return a - b; }

/* calculate the nth percentile */
function percentile(_arr, k) {
	var realIndex = k * (_arr.length - 1);
	var index = parseInt(realIndex);
	var frac = realIndex - index;

	if (index + 1 < _arr.length) {
		return _arr[index] * (1 - frac) + _arr[index + 1] * frac;
	} else {
		return _arr[index];
	}
}

function generate_stats(argument) {
	var min, p1, p5, p50, p95, p99, max, r90, r98;
	var mean, variance = 0, stddev, kurt = 0, skew = 0, loop;
	var dist = argument.slice().sort(ascNum);

	/* number of elements in the array */
	var lenny = dist.length;

	/* first and last should be min and max */
	min = dist[0]
	max = dist[lenny - 1]

	/* calculate percentiles */
	p1 = percentile(dist, 0.01)
	p5 = percentile(dist, 0.05)
	p50 = percentile(dist, 0.5)
	p95 = percentile(dist, 0.95)
	p99 = percentile(dist, 0.99)

	/* calculate ranges */
	r90 = p95 - p5;
	r98 = p99 - p1;

	if (lenny > 0) {
		mean = dist.reduce((a, b) => a + b) / lenny;

		/* some things say length others length - 1 */
		variance = dist.map(x => Math.pow((x - mean), 2))
			       .reduce((a, b) => a + b) /
			   lenny;
		stddev = Math.sqrt(variance);

		skew = dist.map(x => Math.pow((x - mean) / stddev, 3))
			   .reduce((a, b) => a + b) /
		       lenny;
		kurt = dist.map(x => Math.pow((x - mean) / stddev, 4))
			       .reduce((a, b) => a + b) /
			   lenny -
		       3;
	} else {
		mean = NaN;
		variance = NaN;
		stddev = NaN;
		kurt = NaN;
		skew = NaN;
	}
	return [
		min, p1, p5, p50, p95, p99, max, r90, r98, stddev, mean, skew,
		kurt
	];
}

function do_shim() {
	var shimmy = {
		"xaxis" : {"showgrid" : true, "gridcolor" : "rgba(0,0,0,0.5)"}
	};
	layoutClockFreq = Object.assign(layoutClockFreq, shimmy);
	layoutTempFreq = Object.assign(layoutTempFreq, shimmy);
	layoutGPS = Object.assign(layoutGPS, shimmy);
	layoutJitters = Object.assign(layoutJitters, shimmy);
	layoutOffsets = Object.assign(layoutOffsets, shimmy);
	layoutLJitters = Object.assign(layoutLJitters, shimmy);
	/* layout_histo = Object.assign(layout_histo, shimmy); */
}
