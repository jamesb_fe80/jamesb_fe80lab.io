#!/usr/bin/env python
# encoding: utf-8

"""tracker.py -- modified from webgps.py

It tracks the currently visible GPS satellites over a time period.

Usage:
    ./tracker.py

the script never exits and continuously updates the skyview.

tracker.py generates a file "gpsd-c.json" with the satellite tracks.

If tracker.py is interrupted with Ctrl-C before the duration is over, it saves
the current tracks into the file "tracks.j". This is a JSON file.
If this file is present on start of tracker.py, it is loaded. This allows to
restart tracker.py without losing accumulated satellite tracks.
"""

# This file is Copyright by the GPSD project
# SPDX-License-Identifier: BSD-2-clause

from __future__ import absolute_import, print_function, division

import json
import os
import sys

from gps import *

# Untested: any recent gpsd should do

TRACKMAX = 1024
STALECOUNT = 10
JSONFILE = "/tmp/gpsd-c.json"
JFILE = 'tracks.j'

class Track:
    '''Store the track of one satellite.'''

    def __init__(self, prn):
        self.prn = prn
        self.stale = 0
        self.posn = []          # list of (x, y) tuples

    def add(self, x, y):
        pos = (x, y)
        self.stale = STALECOUNT
        if not self.posn or self.posn[0] != pos:
            self.posn.insert(0, pos)
            lenny = len(self.posn)
            if lenny > TRACKMAX:
                print("Dropping record: Track %d too long %d." % (sef.prn, lenny))
                self.posn = self.posn[:TRACKMAX]
            return 1
        return 0

    def track(self):
        '''Return the track as JSON fragment.'''
        return self.posn


class SatTracks(gps):
    '''gpsd client writing JSON output.'''

    def __init__(self):
        super(SatTracks, self).__init__()
        self.sattrack = {}      # maps PRNs to Tracks
        self.state = None
        self.needsupdate = 0

    def json(self, fh):
        # write the tracks
        dictionary = {}
        for t in self.sattrack.values():
            if t.posn:
                dictionary[t.prn] = t.track()
        json.dump(dictionary, fh)

    def make_stale(self):
        for t in self.sattrack.values():
            if t.stale:
                t.stale -= 1

    def delete_stale(self):
        stales = []
        for prn in self.sattrack.keys():
            if self.sattrack[prn].stale == 0:
                stales.append(prn)
                self.needsupdate = 1
        if stales != []:
            print("OLD: Deleting staled track(s) %s" % repr(prn))
        for prn in stales:
            del self.sattrack[prn]

    def insert_sat(self, prn, x, y):
        try:
            t = self.sattrack[prn]
        except KeyError:
            self.sattrack[prn] = t = Track(prn)
        if t.add(x, y):
            self.needsupdate = 1

    def update_tracks(self):
        self.make_stale()
        for s in self.satellites:
            self.insert_sat(s.PRN, s.elevation, s.azimuth)
        self.delete_stale()

    def generate_json(self, jsonfile):
        fh = open(jsonfile, 'w')
        self.json(fh)
        fh.close()

    def run(self):
        jsonfile = JSONFILE
        self.needsupdate = 1
        self.stream(WATCH_ENABLE | WATCH_NEWSTYLE)
        for report in self:
            if report['class'] not in ('SKY'):
                continue
            self.update_tracks()
            if self.needsupdate:
                self.generate_json(jsonfile)
                self.needsupdate = 0


def main():
    sat = SatTracks()

    # restore the tracks
    if os.path.isfile(JFILE):
        print("INIT: Found tracks file.")
        with open(JFILE, 'rb') as j:
            try:
                dictionary = json.load(j)
                for t in dictionary.values():
                    SatTracks[t['prn']] = Track(t['prn'])
                    SatTracks[t.prn]['stale']=t.stale
                    SatTracks[t.prn]['posn']=t.posn
            except ValueError:
                print("tracker.py WARNING: Ignoring incompatible tracks file.", file=sys.stderr)
                quit()
            lens = list(map(lambda k : len(jsob[k].posn), dictionary.keys()))
            print("INIT: Loaded %d buckets with %d point and %d staleness." %
                 sum(lens), len(lens), sum(list(map(lambda k : dictionary[k]['stale']))))

    try:
        sat.run()
    except KeyboardInterrupt:
        pass
    finally:
        # save the tracks
        j = open(JFILE, 'wb')
        dictionary = {}
        for t in sat.sattrack.values():
            dictionary[t.prn] = dict(prn=t.prn, stale=t.stale, posn=t.posn)
        lens = list(map(lambda k : len(dictionary[k]['prn']), dictionary.keys()))
        json.dump(dictionary, j)
        print("Excepted: Saved %d track points in %d buckets w/ %d total staleness." %
              sum(lens), len(lens), sum(list(map(lambda k : dictionary[k]['stale']))))
        j.close()


if __name__ == '__main__':
    main()
