#!/usr/bin/env python
# encoding: utf-8

"""tracker.py -- modified from webgps.py

It tracks the currently visible GPS satellites over a time period.

Usage:
    ./tracker.py [duration]

    duration may be
    - a number of seconds
    - a number followed by a time unit ('s' for secinds, 'm' for minutes,
      'h' for hours or 'd' for days, e.g. '4h' for a duration of four hours)
    - the letter 'c' for continuous operation

If duration is missing, the current skyview is generated and tracker.py exits
immediately. This is the same as giving a duration of 0.

If a duration is given, tracker.py runs for this duration and generates the
tracks of the GPS satellites in view. If the duration is the letter 'c',
the script never exits and continuously updates the skyview.

tracker.py generates a file "gpsd-<duration>.json" with the satellite tracks.

If tracker.py is interrupted with Ctrl-C before the duration is over, it saves
the current tracks into the file "tracks.p". This is a Python "pickle" file.
If this file is present on start of tracker.py, it is loaded. This allows to
restart tracker.py without losing accumulated satellite tracks.
"""

# This file is Copyright (c) 2010-2019 by the GPSD project
# SPDX-License-Identifier: BSD-2-clause

from __future__ import absolute_import, print_function, division

import json
import math
import os
import pickle
import sys
import time

from gps import *

gps_version = '3.19-dev'
if gps.__version__ != gps_version:
    sys.stderr.write("tracker.py: ERROR: need gps module version %s, got %s\n" %
                     (gps_version, gps.__version__))
    sys.exit(1)


TRACKMAX = 1024
STALECOUNT = 10


class Track:
    '''Store the track of one satellite.'''

    def __init__(self, prn):
        self.prn = prn
        self.stale = 0
        self.posn = []          # list of (x, y) tuples

    def add(self, x, y):
        pos = (x, y)
        self.stale = STALECOUNT
        if not self.posn or self.posn[0] != pos:
            self.posn.insert(0, pos)
            if len(self.posn) > TRACKMAX:
                self.posn = self.posn[:TRACKMAX]
            return 1
        return 0

    def track(self):
        '''Return the track as JSON fragment.'''
	return self.posn


class SatTracks(gps):
    '''gpsd client writing JSON output.'''

    def __init__(self):
        super(SatTracks, self).__init__()
        self.sattrack = {}      # maps PRNs to Tracks
        self.state = None
        self.statetimer = time.time()
        self.needsupdate = 0

    def json(self, fh):
        # write the tracks
        dictionary = {}
        for t in self.sattrack.values():
            if t.posn:
                dictionary[t.prn] = t.track()
        json.dump(dictionary, fh)

    def make_stale(self):
        for t in self.sattrack.values():
            if t.stale:
                t.stale -= 1

    def delete_stale(self):
        stales = []
        for prn in self.sattrack.keys():
            if self.sattrack[prn].stale == 0:
                stales.append(prn)
                self.needsupdate = 1
        for prn in stales:
            del self.sattrack[prn]

    def insert_sat(self, prn, x, y):
        try:
            t = self.sattrack[prn]
        except KeyError:
            self.sattrack[prn] = t = Track(prn)
        if t.add(x, y):
            self.needsupdate = 1

    def update_tracks(self):
        self.make_stale()
        for s in self.satellites:
            self.insert_sat(s.PRN, s.elevation, s.azimuth)
        self.delete_stale()

    def generate_json(self, jsonfile):
        fh = open(jsonfile, 'w')
        self.json(fh)
        fh.close()

    def run(self, suffix, period):
        jsonfile = 'gpsd' + suffix + '.json'
        if period is not None:
            end = time.time() + period
        self.needsupdate = 1
        self.stream(WATCH_ENABLE | WATCH_NEWSTYLE)
        for report in self:
            if report['class'] not in ('SKY'):
                continue
            self.update_tracks()
            if self.needsupdate:
                self.generate_json(jsonfile)
                self.needsupdate = 0
            if period is not None and (
                period <= 0 and self.fix.mode >= MODE_2D or
                period > 0 and time.time() > end
            ):
                break


def main():
    argv = sys.argv[1:]

    factors = {
        's': 1, 'm': 60, 'h': 60 * 60, 'd': 24 * 60 * 60
    }
    arg = argv and argv[0] or '0'
    if arg[-1:] in factors.keys():
        period = int(arg[:-1]) * factors[arg[-1]]
    elif arg == 'c':
        period = None
    else:
        period = int(arg)
    prefix = '-' + arg

    sat = SatTracks()

    # restore the tracks
    pfile = 'tracks.p'
    if os.path.isfile(pfile):
        p = open(pfile, 'rb')
        try:
            sat.sattrack = pickle.load(p)
        except ValueError:
            print("Ignoring incompatible tracks file.", file=sys.stderr)
        p.close()

    try:
        sat.run(prefix, period)
    except KeyboardInterrupt:
        # save the tracks
        p = open(pfile, 'wb')
        # No pickle is backward-compatible from Python 3 to Python 2,
        # so we just use the default and punt at load time if needed.
        pickle.dump(sat.sattrack, p)
        p.close()


if __name__ == '__main__':
    main()
