#! /usr/bin/python3
import ctypes
from ctypes.util import find_library
import json
import os

import gps

C = ctypes.CDLL(find_library("c"))
C.shm_open.res_type = ctypes.c_int
C.shm_open.arg_types = [ctypes.c_char_p, ctypes.c_int, ctypes.c_int]
# os.close()
# os.ftruncate
# os.lseek
# os.fstat


if __name__ == "__main__":
    clip_digits = 3
    tpv_clip = [
        "magtrack",
        "track",
        "lat",
        "lon",
        "alt",
        "altHAE",
        "altMSL",
        "epx",
        "epy",
        "epv",
        "eps",
        "epd",
        "epc",
        "ept",
        "speed",
        "climb",
    ]
    sky_clip = ["xdop", "ydop", "vdop", "cdop", "sdop", "edop", "tdop"]

    print(
        """\
Content-Type: text/event-stream
Access-Control-Allow-Origin: *
Cache-Control: no-cache
"""
    )

    try:
        points = []
        try:
            fd1 = C.shm_open("gpsdc-tracks", os.O_RDONLY, 0o644)
            points = json.loads(os.read(fd1, 1 << 16).split(b"\0")[0])
            os.close(fd1)
        except (
            json.decoder.JSONDecodeError
        ):
            pass

        inny = {"class": "inital_tracking", "points": points}
        print("data: %s\n" % json.dumps(inny), flush=True)

        session = gps.gps(device='/dev/ttyUSB0')
        session.stream(gps.WATCH_ENABLE | gps.WATCH_NEWSTYLE)

        for _ in session:
            inny = json.loads(session.response)
            clips = []
            if inny["class"] == "TPV" and inny['device'] == '/dev/ttyUSB0':
                clips = tpv_clip
            elif inny["class"] == "SKY" and inny['device'] == '/dev/ttyUSB0':
                clips = sky_clip
            for key in clips:
                if key in inny:
                    inny[key] = round(inny[key], clip_digits)
                inny["class"] = inny["class"].lower()
            print("data: %s\n" % json.dumps(dict(inny)), flush=True)
    except KeyboardInterrupt:
        os.close(fd1)
